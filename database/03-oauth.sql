create table oauth_client_details (
    client_id varchar(256) primary key,
    client_secret varchar(256),
    resource_ids varchar(256),
    scope varchar(256),
    authorized_grant_types varchar(256),
    web_server_redirect_uri varchar(256),
    authorities varchar(256),
    access_token_validity integer,
    refresh_token_validity integer,
    additional_information varchar(4096),
    autoapprove varchar(256)
);

create table oauth_access_token (
  token_id VARCHAR(256),
  token BYTEA,
  authentication_id VARCHAR(256) PRIMARY KEY,
  user_name VARCHAR(256),
  client_id VARCHAR(256),
  authentication BYTEA,
  refresh_token VARCHAR(256)
);

create table oauth_refresh_token (
  token_id VARCHAR(256),
  token BYTEA,
  authentication BYTEA
);

INSERT INTO oauth_client_details (client_id, client_secret, scope, authorized_grant_types, access_token_validity, refresh_token_validity)
     VALUES ('simplefer', '$2a$10$Xs7hbG04QVbMMMqqjffp6.DydNeDxjpbQ.f1nA/rXu4YkSAggJ5Rm', 'read,write', 'authorization_code,check_token,refresh_token,password', 600, 43200);
