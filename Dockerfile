FROM maven:3.6.3-openjdk-11

# Copy sources
COPY simplefer /tmp/simplefer

# Build the application
RUN cd /tmp/simplefer && mvn clean install && \
# Copy generated jar to /home/app/simplefer.jar
     cd target && APP_JAR=`ls simplefer*.jar` && \
     mkdir /home/app && mv $APP_JAR /home/app/simplefer.jar && \
     chmod a+x /home/app/simplefer.jar && \
# Cleanup
     cd /tmp && rm -rf simplefer

CMD ["java", "-jar", "/home/app/simplefer.jar", "--spring.profiles.active=postgres"]