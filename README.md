# Simplefer

Simplefer is a simple playground payment REST API using Spring Boot 2 and Java 11.

Authentication has been implemented using OAuth2 opaque token with Resource and Authorization server within the same application.
Currently, there are 2 possible roles for a user: 

* USER: Can retrieve their own data and create payments with own giver account.
* ADMIN: has USER access and can also manage dictionary data and create new users.

The application has the following features:

* Authentication using OAuth2 with user/password from database.
* List of own Bank accounts with pagination and sorting.
* Create a single payment:
    * Giver account must belong to authenticated user.
    * Disallow payments to same account.
    * Balance validation.
    * Validation against sanctioned accounts - blocking user's bank account after 5 payment tentative to sanctioned accounts.
    * If beneficiary is also in Simplefer database, transfer funds from one account to another, otherwise just deduct from giver account.
    * Execution date can be specified with the current date, or a future date for a scheduled payment.
        * Scheduled payments are executed using spring scheduler that runs every hour checking for scheduled payments.
* List own payments with pagination and sorting.
* List own payments to specific beneficiary with pagination, sorting and date filtering.
* Delete scheduled payment - Payments other than scheduled cannot be deleted.
* Return payment - if user is beneficiary of an Executed payment, he is able to return it to giver account.
* Update user information - Password and address can be updated.
* Logout operation revoking authentication and refresh tokens.
* Swagger documentation: http://localhost:8080/swagger-ui/index.html
* Aspect logging for debug mode
* Admin only features:
    * List of Bank accounts by user with pagination and sorting.
    * Currency CRUD operations.
    * Sanction CRUD operations.
    * User creation and list.

## Requirements
This application has been developed and tested using the following dependencies versions*:

* Maven 3.6.3
* Java 11.0.2-open
* Postgres 13.2
* Docker 20.10.2
* Docker-compose 1.27.4

*It does not mean that you will need exactly these versions, but same or greater is advisable.

** If running on Docker, only Docker and Docker-compose are required, of course.

## Build and Run
There are 2 options to build the application. Using Docker, or Maven.

The application contains 2 profiles to run:

* default: Runs the application in memory using H2 database.
* postgres: Runs the application with Postgres database connection (db:5432/simplefer).

### Maven in memory setup
From simplefer maven project folder issue the following command:

```
mvn clean install
```

After all dependencies has been correctly fetch and project built, you can run the following to start the application in memory:

```
java -jar target/simplefer*.jar
```

You can now access the REST endpoints on:
http://localhost:8080/api/*

### Docker setup with Postgres
From the root folder issue the command:
```
docker-compose -p simplefer up
```

An image for the REST API will be build and 2 containers should start:

* simplefer_web_1 with simplefer application running with default profile.
* simplefer_db_1 with Postgres instance.

You can now access the REST endpoints on:
http://localhost:8080/api/*

## Consuming endpoints
Both in memory and Docker versions start with some sample initial data. The initial available users are:

| Username   | Password | Role  |
|------------|----------|-------|
| admin      | password | ADMIN |
| user1      | password | USER  |
| user2      | password | USER  |

The whole application requires authentication (besides Swagger UI), therefore one must retrieve OAuth authentication token before calling any endpoint.
To do so, you can issue the following request:

Retrieve new authentication and refresh token:
```
curl --data "username=admin&password=password&grant_type=password&scope=read write" --user simplefer:password http://localhost:8080/oauth/token
```

Retrieve new authentication using refresh token:
```
curl --data "refresh_token=cd58a5cd-ffe9-4609-b023-90c5f53fa83b&grant_type=refresh_token&scope=read write" --user simplefer:password http://localhost:8080/oauth/token
```

On the subsequent requests you should set the Authorization header with the access_token value.
In case of http verbs POST or PUT, Content-type:application/json header is also required. 
Ie.:

```
curl -X GET --header "Authorization: Bearer ed980dac-7a86-40d0-9695-a2703577d366" http://localhost:8080/api/currencies
```
```
curl -X POST --data '{"code": "BRL"}' --header "Authorization: Bearer ed980dac-7a86-40d0-9695-a2703577d366" --header "Content-type: application/json" http://localhost:8080/api/currencies
```
```
curl -X PUT --data '{"code": "EURO"}' --header "Authorization: Bearer ed980dac-7a86-40d0-9695-a2703577d366" --header "Content-type: application/json" http://localhost:8080/api/currencies/1
```
```
curl -X DELETE --header "Authorization: Bearer ed980dac-7a86-40d0-9695-a2703577d366" http://localhost:8080/api/currencies/4
```

### Swagger-ui
Alternatively you can also use Swagger UI for easier API browsing/testing.

http://localhost:8080/swagger-ui/index.html

Firstly on the main page click on Authorize button and provide authentication details.
Then you are good to go and use the endpoints.

## Aspect Logging
Currently, logging has been set as INFO level, if set to lower level as TRACE or DEBUG, LoggingAspect will log debug every 
method execution as well as exceptions. This can be done on application.yml logging section. 

## Future enhancements
* Add database indexes for better performance.
* Add some database version control (ie: Flyway, Liquibase), and have database setup agnostic.
* Separate OAuth2 Authorization server from resource.
* Create Integration tests.
* Fix security on controller tests - currently security is disabled for testing.
* Add i18n - Somehow intercept Locale to use on ConstraintExceptionFactory.
* Create Balance list endpoint.
* Use soft delete instead of hard.
* Create admin CRUD for bank accounts.
* Create admin endpoint to link bank account to user.
* Use OAuth authorization scope accordingly.
* Configure the application to run over https