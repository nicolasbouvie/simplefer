package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.dto.CurrencyDto;
import dev.nicolasbouvie.simplefer.data.mapper.CurrencyMapper;
import dev.nicolasbouvie.simplefer.data.mapper.CurrencyMapperImpl;
import dev.nicolasbouvie.simplefer.data.model.Currency;
import dev.nicolasbouvie.simplefer.service.CurrencyService;
import dev.nicolasbouvie.simplefer.test.util.ControllerTest;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ControllerTest(CurrencyController.class)
public class CurrencyControllerTest extends AbstractControllerTest<CurrencyService> {

    @MockBean(answer = Answers.CALLS_REAL_METHODS, classes = {CurrencyMapperImpl.class})
    private CurrencyMapper mapper;

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void list() throws Exception {
        var model = new PageImpl<>(List.of(RandomObject.nextObject(Currency.class)));
        var expected = pageMapper.toDto(model, mapper);

        Mockito.when(service.list(Mockito.any())).thenReturn(model);

        mvc.perform(get("/api/currencies"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void getById() throws Exception {
        var id = RandomObject.nextObject(Long.class);
        var model = RandomObject.nextObject(Currency.class);
        var expected = mapper.toDto(model);

        Mockito.when(service.get(id)).thenReturn(model);

        mvc.perform(get("/api/currencies/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "admin", roles = {"ADMIN"})
    public void create() throws Exception {
        var dto = RandomObject.nextObject(CurrencyDto.class);
        var model = RandomObject.nextObject(Currency.class);
        var expected = mapper.toDto(model);

        Mockito.when(service.create(Mockito.any())).thenReturn(model);

        mvc.perform(post("/api/currencies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void update() throws Exception {
        var dto = RandomObject.nextObject(CurrencyDto.class);
        var model = RandomObject.nextObject(Currency.class);
        var expected = mapper.toDto(model);

        Mockito.when(service.update(Mockito.any())).thenReturn(model);

        mvc.perform(put("/api/currencies/{id}", dto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void deleteById() throws Exception {
        var id = RandomObject.nextObject(Long.class);

        mvc.perform(delete("/api/currencies/{id}", id)).andExpect(status().isOk());
    }
}
