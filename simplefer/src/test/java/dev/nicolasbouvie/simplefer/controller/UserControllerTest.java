package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.dto.UserDto;
import dev.nicolasbouvie.simplefer.data.dto.UserInfoDto;
import dev.nicolasbouvie.simplefer.data.mapper.UserMapper;
import dev.nicolasbouvie.simplefer.data.mapper.UserMapperImpl;
import dev.nicolasbouvie.simplefer.data.model.User;
import dev.nicolasbouvie.simplefer.service.UserService;
import dev.nicolasbouvie.simplefer.test.util.ControllerTest;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ControllerTest(UserController.class)
public class UserControllerTest extends AbstractControllerTest<UserService> {

    @MockBean(answer = Answers.CALLS_REAL_METHODS, classes = {UserMapperImpl.class})
    private UserMapper mapper;

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void list() throws Exception {
        var model = new PageImpl<>(List.of(RandomObject.nextObject(User.class)));
        var expected = pageMapper.toDto(model, mapper);

        Mockito.when(service.list(Mockito.any())).thenReturn(model);

        mvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void getById() throws Exception {
        var id = RandomObject.nextObject(Long.class);
        var model = RandomObject.nextObject(User.class);
        var expected = mapper.toDto(model);

        Mockito.when(service.get(id)).thenReturn(model);

        mvc.perform(get("/api/users/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "admin", roles = {"ADMIN"})
    public void create() throws Exception {
        var dto = RandomObject.nextObject(UserDto.class);
        var model = RandomObject.nextObject(User.class);
        var expected = mapper.toDto(model);

        Mockito.when(service.create(Mockito.any())).thenReturn(model);

        mvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void updateUserInfo() throws Exception {
        var dto = RandomObject.nextObject(UserInfoDto.class);
        var model = RandomObject.nextObject(User.class);
        var expected = mapper.toDto(model);

        Mockito.when(service.updateInfo(Mockito.any())).thenReturn(model);

        mvc.perform(put("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }


    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void logout() throws Exception {
        mvc.perform(post("/api/logout")).andExpect(status().isOk());
    }
}
