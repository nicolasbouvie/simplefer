package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.dto.PaymentDto;
import dev.nicolasbouvie.simplefer.data.mapper.PaymentMapper;
import dev.nicolasbouvie.simplefer.data.mapper.PaymentMapperImpl;
import dev.nicolasbouvie.simplefer.data.model.Payment;
import dev.nicolasbouvie.simplefer.service.IBANService;
import dev.nicolasbouvie.simplefer.service.PaymentService;
import dev.nicolasbouvie.simplefer.test.util.ControllerTest;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ControllerTest(PaymentController.class)
public class PaymentControllerTest extends AbstractControllerTest<PaymentService> {

    @MockBean(answer = Answers.CALLS_REAL_METHODS, classes = {PaymentMapperImpl.class})
    private PaymentMapper mapper;

    @MockBean
    private IBANService ibanService;

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void list() throws Exception {
        var model = new PageImpl<>(List.of(RandomObject.nextObject(Payment.class)));
        var expected = pageMapper.toDto(model, mapper);

        Mockito.when(service.list(Mockito.any())).thenReturn(model);

        mvc.perform(get("/api/payments"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void listByBeneficiary() throws Exception {
        var iban = RandomObject.nextObject(String.class);
        var model = new PageImpl<>(List.of(RandomObject.nextObject(Payment.class)));
        var expected = pageMapper.toDto(model, mapper);

        Mockito.when(service.listByBeneficiary(Mockito.any(), Mockito.any())).thenReturn(model);

        mvc.perform(get("/api/payments/beneficiaryAccount/{accountNumber}", iban))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void create() throws Exception {
        var dto = RandomObject.nextObject(PaymentDto.class);
        var model = RandomObject.nextObject(Payment.class);
        var expected = mapper.toDto(model);

        Mockito.when(ibanService.isValid(Mockito.any())).thenReturn(true);
        Mockito.when(service.create(Mockito.any())).thenReturn(model);

        mvc.perform(post("/api/payments")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void returnPayment() throws Exception {
        var id = RandomObject.nextObject(Long.class);
        var model = RandomObject.nextObject(Payment.class);
        var expected = mapper.toDto(model);

        Mockito.when(service.returnPayment(id)).thenReturn(model);

        mvc.perform(put("/api/payments/{id}/return", id)
                    .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void deleteById() throws Exception {
        var id = RandomObject.nextObject(Long.class);

        mvc.perform(delete("/api/payments/{id}", id)).andExpect(status().isOk());
    }
}
