package dev.nicolasbouvie.simplefer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.nicolasbouvie.simplefer.data.mapper.PageMapper;
import dev.nicolasbouvie.simplefer.data.mapper.PageMapperImpl;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

public abstract class AbstractControllerTest<T> {

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @MockBean
    protected T service;

    @MockBean(answer = Answers.CALLS_REAL_METHODS, classes = {PageMapperImpl.class})
    protected PageMapper pageMapper;

    @MockBean
    protected ConstraintExceptionFactory constraintExceptionFactory;

}
