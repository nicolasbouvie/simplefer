package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.mapper.BankAccountMapper;
import dev.nicolasbouvie.simplefer.data.mapper.BankAccountMapperImpl;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.service.BankAccountService;
import dev.nicolasbouvie.simplefer.test.util.ControllerTest;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ControllerTest(BankAccountController.class)
public class BankAccountControllerTest extends AbstractControllerTest<BankAccountService> {

    @MockBean(answer = Answers.CALLS_REAL_METHODS, classes = {BankAccountMapperImpl.class})
    private BankAccountMapper mapper;

    @Test
    @WithMockUser(username = "user", authorities = {"USER"})
    public void list() throws Exception {
        var model = new PageImpl<>(List.of(RandomObject.nextObject(BankAccount.class)));
        var expected = pageMapper.toDto(model, mapper);

        Mockito.when(service.list(Mockito.any())).thenReturn(model);

        mvc.perform(get("/api/accounts"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void listUserAccounts() throws Exception {
        var model = new PageImpl<>(List.of(RandomObject.nextObject(BankAccount.class)));
        var expected = pageMapper.toDto(model, mapper);

        Mockito.when(service.listByUser(Mockito.any(), Mockito.any())).thenReturn(model);

        mvc.perform(get("/api/accounts/{username}", "user"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expected)));
    }
}
