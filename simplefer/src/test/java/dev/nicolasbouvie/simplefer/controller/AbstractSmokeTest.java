package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.test.util.ApplicationTest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;

@ApplicationTest
public abstract class AbstractSmokeTest<T extends AbstractController<?, ?>> {

    @Autowired
    private T controller;

    @Test
    public void contextLoads() {
        Assertions.assertThat(controller).isNotNull();
    }

    @Test
    public void requiredPropertiesInitialized() {
        for (String requiredProperty : requiredProperties()) {
            Assertions.assertThat(controller).hasFieldOrProperty(requiredProperty).isNotNull();
        }
    }

    protected Collection<String> requiredProperties() {
        return List.of("service", "mapper", "pageMapper");
    }
}
