package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.BalanceDto;
import dev.nicolasbouvie.simplefer.data.model.Balance;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class BalanceMapperTest {

    private final BalanceMapper mapper = Mappers.getMapper(BalanceMapper.class);

    @Test
    public void toModel() {
        var dto = RandomObject.nextObject(BalanceDto.class);
        dto.setType(Balance.Type.AVAILABLE.name());

        var model = mapper.toModel(dto);
        assertThat(model).isNotNull();
        assertThat(model.getId()).isEqualTo(dto.getId());
        assertThat(model.getAmount()).isEqualTo(BigDecimal.valueOf(dto.getAmount()));
        assertThat(model.getCurrency()).isNotNull();
        assertThat(model.getCurrency().getCode()).isEqualTo(dto.getCurrency());
        assertThat(model.getType()).isEqualTo(Balance.Type.AVAILABLE);
        assertThat(model.getBankAccount()).isNotNull();
        assertThat(model.getBankAccount().getAccountNumber()).isEqualTo(dto.getAccountNumber());
    }

    @Test
    public void toDto() {
        var model = RandomObject.nextObject(Balance.class);

        var dto = mapper.toDto(model);
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(model.getId());
        assertThat(dto.getAmount()).isEqualTo(model.getAmount().doubleValue());
        assertThat(dto.getCurrency()).isNotNull();
        assertThat(dto.getCurrency()).isEqualTo(model.getCurrency().getCode());
        assertThat(dto.getType()).isEqualTo(model.getType().toString());
        assertThat(dto.getAccountNumber()).isEqualTo(model.getBankAccount().getAccountNumber());
    }
}
