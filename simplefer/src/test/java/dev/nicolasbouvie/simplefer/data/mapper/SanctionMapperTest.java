package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.SanctionDto;
import dev.nicolasbouvie.simplefer.data.model.Sanction;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class SanctionMapperTest {

    private final SanctionMapper mapper = Mappers.getMapper(SanctionMapper.class);

    @Test
    public void toModel() {
        var dto = RandomObject.nextObject(SanctionDto.class);

        var model = mapper.toModel(dto);
        assertThat(model).isNotNull();
        assertThat(model.getId()).isEqualTo(dto.getId());
        assertThat(model.getAccountNumber()).isEqualTo(dto.getAccountNumber());
        assertThat(model.getReason()).isEqualTo(dto.getReason());
    }

    @Test
    public void toDto() {
        var model = RandomObject.nextObject(Sanction.class);

        var dto = mapper.toDto(model);
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(model.getId());
        assertThat(dto.getAccountNumber()).isEqualTo(model.getAccountNumber());
        assertThat(dto.getReason()).isEqualTo(model.getReason());
    }
}
