package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.PaymentDto;
import dev.nicolasbouvie.simplefer.data.model.Payment;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class PaymentMapperTest {

    private final PaymentMapper mapper = Mappers.getMapper(PaymentMapper.class);

    @Test
    public void toModel() {
        var dto = RandomObject.nextObject(PaymentDto.class);
        dto.setStatus(Payment.Status.EXECUTED.name());
        dto.setType(Payment.Type.PAYMENT.name());

        var model = mapper.toModel(dto);
        assertThat(model).isNotNull();
        assertThat(model.getId()).isEqualTo(dto.getId());
        assertThat(model.getAmount()).isEqualTo(BigDecimal.valueOf(dto.getAmount()));
        assertThat(model.getCurrency()).isNotNull();
        assertThat(model.getCurrency().getCode()).isEqualTo(dto.getCurrency());
        assertThat(model.getGiverAccount()).isNotNull();
        assertThat(model.getGiverAccount().getAccountNumber()).isEqualTo(dto.getGiverAccountNumber());
        assertThat(model.getBeneficiaryAccountNumber()).isEqualTo(dto.getBeneficiaryAccountNumber());
        assertThat(model.getBeneficiaryName()).isEqualTo(dto.getBeneficiaryName());
        assertThat(model.getCommunication()).isEqualTo(dto.getCommunication());
        assertThat(model.getCreationDate()).isEqualTo(dto.getCreationDate());
        assertThat(model.getStatus()).isEqualTo(Payment.Status.EXECUTED);
        assertThat(model.getType()).isEqualTo(Payment.Type.PAYMENT);
        assertThat(model.getExecutionDate()).isEqualTo(dto.getExecutionDate());
    }

    @Test
    public void toDto() {
        var model = RandomObject.nextObject(Payment.class);

        var dto = mapper.toDto(model);
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(model.getId());
        assertThat(dto.getAmount()).isEqualTo(model.getAmount().doubleValue());
        assertThat(dto.getCurrency()).isEqualTo(model.getCurrency().getCode());
        assertThat(dto.getGiverAccountNumber()).isEqualTo(model.getGiverAccount().getAccountNumber());
        assertThat(dto.getBeneficiaryAccountNumber()).isEqualTo(model.getBeneficiaryAccountNumber());
        assertThat(dto.getBeneficiaryName()).isEqualTo(model.getBeneficiaryName());
        assertThat(dto.getCommunication()).isEqualTo(model.getCommunication());
        assertThat(dto.getCreationDate()).isEqualTo(model.getCreationDate());
        assertThat(dto.getStatus()).isEqualTo(Payment.Status.EXECUTED.name());
        assertThat(dto.getType()).isEqualTo(Payment.Type.PAYMENT.name());
        assertThat(dto.getExecutionDate()).isEqualTo(model.getExecutionDate());
    }
}
