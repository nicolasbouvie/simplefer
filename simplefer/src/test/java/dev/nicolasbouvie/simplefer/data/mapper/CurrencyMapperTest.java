package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.CurrencyDto;
import dev.nicolasbouvie.simplefer.data.model.Currency;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class CurrencyMapperTest {

    private final CurrencyMapper mapper = Mappers.getMapper(CurrencyMapper.class);

    @Test
    public void toModel() {
        var dto = RandomObject.nextObject(CurrencyDto.class);

        var model = mapper.toModel(dto);
        assertThat(model).isNotNull();
        assertThat(model.getId()).isEqualTo(dto.getId());
        assertThat(model.getCode()).isEqualTo(dto.getCode());
    }

    @Test
    public void toDto() {
        var model = RandomObject.nextObject(Currency.class);

        var dto = mapper.toDto(model);
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(model.getId());
        assertThat(dto.getCode()).isEqualTo(model.getCode());
    }
}
