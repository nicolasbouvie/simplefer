package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.BankAccountDto;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class BankAccountMapperTest {

    private final BankAccountMapper mapper = Mappers.getMapper(BankAccountMapper.class);

    @Test
    public void toModel() {
        var dto = RandomObject.nextObject(BankAccountDto.class);
        dto.setStatus(BankAccount.Status.ENABLE.name());

        var model = mapper.toModel(dto);
        assertThat(model).isNotNull();
        assertThat(model.getId()).isEqualTo(dto.getId());
        assertThat(model.getAccountNumber()).isEqualTo(dto.getAccountNumber());
        assertThat(model.getAccountName()).isEqualTo(dto.getAccountName());
        assertThat(model.getStatus()).isEqualTo(BankAccount.Status.ENABLE);
        assertThat(model.getUsers()).hasSize(1);
        assertThat(model.getUsers()).first()
                .matches(u->dto.getUsers().iterator().next().equals(u.getUsername()));
    }

    @Test
    public void toDto() {
        var model = RandomObject.nextObject(BankAccount.class);

        var dto = mapper.toDto(model);
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(model.getId());
        assertThat(dto.getAccountNumber()).isEqualTo(model.getAccountNumber());
        assertThat(dto.getAccountName()).isEqualTo(model.getAccountName());
        assertThat(dto.getStatus()).isEqualTo(model.getStatus().name());
        assertThat(dto.getUsers()).hasSize(1);
        assertThat(dto.getUsers()).first()
                .matches(u->model.getUsers().iterator().next().getUsername().equals(u));
    }
}
