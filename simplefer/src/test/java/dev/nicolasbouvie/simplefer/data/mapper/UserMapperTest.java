package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.UserDto;
import dev.nicolasbouvie.simplefer.data.model.User;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class UserMapperTest {

    private final UserMapper mapper = Mappers.getMapper(UserMapper.class);

    @Test
    public void toModel() {
        var dto = RandomObject.nextObject(UserDto.class);

        var model = mapper.toModel(dto);
        assertThat(model).isNotNull();
        assertThat(model.getId()).isEqualTo(dto.getId());
        assertThat(model.getUsername()).isEqualTo(dto.getUsername());
        assertThat(model.getPassword()).isEqualTo(dto.getPassword());
        assertThat(model.getAddress()).isEqualTo(dto.getAddress());
        assertThat(model.getBankAccounts()).hasSize(1);
        assertThat(model.getBankAccounts()).first()
                .matches(a->dto.getBankAccounts().iterator().next().equals(a.getAccountNumber()));
        assertThat(model.getRoles()).hasSize(1);
        assertThat(model.getRoles()).first()
                .matches(r->dto.getRoles().iterator().next().equals(r.getName()));
    }

    @Test
    public void toDto() {
        var model = RandomObject.nextObject(User.class);

        var dto = mapper.toDto(model);
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(model.getId());
        assertThat(dto.getUsername()).isEqualTo(model.getUsername());
        assertThat(dto.getPassword()).isNull();
        assertThat(dto.getAddress()).isEqualTo(model.getAddress());
        assertThat(dto.getBankAccounts()).hasSize(1);
        assertThat(dto.getBankAccounts()).first()
                .matches(a->model.getBankAccounts().iterator().next().getAccountNumber().equals(a));
        assertThat(dto.getRoles()).hasSize(1);
        assertThat(dto.getRoles()).first()
                .matches(r->model.getRoles().iterator().next().getName().equals(r));
    }
}
