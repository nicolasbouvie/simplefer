package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Payment;
import dev.nicolasbouvie.simplefer.data.model.Sanction;
import dev.nicolasbouvie.simplefer.data.repository.PaymentRepository;
import dev.nicolasbouvie.simplefer.data.repository.SanctionRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FraudCheckServiceImplTest {

    @InjectMocks
    private FraudCheckServiceImpl target;

    @Mock
    private SanctionRepository sanctionRepository;

    @Spy
    private PaymentRepository paymentRepository;

    @Mock
    private ConstraintExceptionFactory constraintExceptionFactory;

    @Test
    public void checkPayment_noSanction() {
        var payment = RandomObject.nextObject(Payment.class);
        when(sanctionRepository.findByAccountNumber(payment.getBeneficiaryAccountNumber()))
                .thenReturn(Optional.empty());

        target.checkPayment(payment);

        verify(paymentRepository, never()).countSuspiciousPayment(any());
    }

    @Test
    public void checkPayment_sanctionAccount() {
        var payment = RandomObject.nextObject(Payment.class);
        payment.getGiverAccount().setStatus(BankAccount.Status.ENABLE);
        var sanction = RandomObject.nextObject(Sanction.class);
        var expected = new ConstraintViolationException(null);

        when(sanctionRepository.findByAccountNumber(payment.getBeneficiaryAccountNumber()))
                .thenReturn(Optional.of(sanction));
        when(paymentRepository.countSuspiciousPayment(payment.getGiverAccount()))
                .thenReturn(-2L);
        when(constraintExceptionFactory.newConstraintValidationException("fraudCheck", sanction.getReason()))
                .thenReturn(expected);

        ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.checkPayment(payment));

        assertThat(actual).isNotNull().isEqualTo(expected);
        assertThat(payment.getGiverAccount().getStatus()).isEqualTo(BankAccount.Status.ENABLE);
        verify(paymentRepository, times(1)).save(payment);
    }

    @Test
    public void checkPayment_sanctionAccountAndBlockAccount() {
        var payment = RandomObject.nextObject(Payment.class);
        payment.getGiverAccount().setStatus(BankAccount.Status.ENABLE);
        var sanction = RandomObject.nextObject(Sanction.class);
        var expected = new ConstraintViolationException(null);

        when(sanctionRepository.findByAccountNumber(payment.getBeneficiaryAccountNumber()))
                .thenReturn(Optional.of(sanction));
        when(paymentRepository.countSuspiciousPayment(payment.getGiverAccount()))
                .thenReturn(5L);
        when(constraintExceptionFactory.newConstraintValidationException("fraudCheck", sanction.getReason()))
                .thenReturn(expected);

        ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.checkPayment(payment));

        assertThat(actual).isNotNull().isEqualTo(expected);
        assertThat(payment.getGiverAccount().getStatus()).isEqualTo(BankAccount.Status.BLOCKED);
        verify(paymentRepository, times(1)).save(payment);
    }
}
