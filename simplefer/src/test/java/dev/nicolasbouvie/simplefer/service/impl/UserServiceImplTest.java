package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.UserDto;
import dev.nicolasbouvie.simplefer.data.dto.UserInfoDto;
import dev.nicolasbouvie.simplefer.data.mapper.UserMapper;
import dev.nicolasbouvie.simplefer.data.model.Role;
import dev.nicolasbouvie.simplefer.data.model.User;
import dev.nicolasbouvie.simplefer.data.repository.RoleRepository;
import dev.nicolasbouvie.simplefer.data.repository.UserRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import dev.nicolasbouvie.simplefer.service.SecurityService;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl target;

    @Spy
    private UserRepository repository;

    @Spy
    private UserMapper mapper;

    @Mock
    private ConstraintExceptionFactory constraintExceptionFactory;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private SecurityService securityService;

    @Mock
    private TokenStore tokenStore;

    @Spy
    private ConsumerTokenServices tokenServices;

    @Test
    public void list_emptyFilter() {
        var filter = new BasePageFilter();

        target.list(filter);

        verify(repository, times(1)).findAll(filter.getPageable("id"));
    }

    @Test
    public void list_nonEmptyFilter() {
        var filter = RandomObject.nextObject(BasePageFilter.class);

        target.list(filter);

        verify(repository, times(1)).findAll(filter.getPageable("shouldUseFromFilter"));
    }

    @Test
    public void get() {
        var id = RandomObject.nextObject(Long.class);
        var expected = RandomObject.nextObject(User.class);

        when(repository.findById(id)).thenReturn(Optional.of(expected));

        User actual = target.get(id);

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void get_notFound_throwsEntityNotFoundException() {
        var id = RandomObject.nextObject(Long.class);
        var expected = new EntityNotFoundException("User not found");

        when(repository.findById(id)).thenReturn(Optional.empty());
        when(constraintExceptionFactory.newEntityNotFoundException("User", id)).thenReturn(expected);

        EntityNotFoundException actual = assertThrows(EntityNotFoundException.class, () -> target.get(id));

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void create() {
        var dto = RandomObject.nextObject(UserDto.class);
        dto.setId(null);

        when(roleRepository.findByName(dto.getRoles().stream().findFirst().get())).thenReturn(Optional.of(new Role()));
        when(mapper.toModel(dto)).thenReturn(Mappers.getMapper(UserMapper.class).toModel(dto));

        target.create(dto);

        verify(mapper, times(1)).toModel(dto);
        verify(repository, times(1)).save(any());
    }

    @Test
    public void create_roleNotFound_throwsEntityNotFoundException() {
        var dto = RandomObject.nextObject(UserDto.class);
        dto.setId(null);
        var role = dto.getRoles().stream().findFirst().get();

        var expected = new EntityNotFoundException("Role not found");

        when(constraintExceptionFactory.newEntityNotFoundException("Role", role)).thenReturn(expected);
        when(roleRepository.findByName(role)).thenReturn(Optional.empty());
        when(mapper.toModel(dto)).thenReturn(Mappers.getMapper(UserMapper.class).toModel(dto));

        EntityNotFoundException actual = assertThrows(EntityNotFoundException.class, () -> target.create(dto));
        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void update() {
        var dto = RandomObject.nextObject(UserInfoDto.class);
        var expected = RandomObject.nextObject(User.class);

        when(repository.findByUsername(any())).thenReturn(Optional.of(expected));
        when(repository.save(any())).thenReturn(expected);
        when(securityService.getAuthentication()).thenReturn(Mockito.mock(OAuth2Authentication.class));
        when(securityService.getAuthentication().getName()).thenReturn("admin");
        when(passwordEncoder.matches(any(), any())).thenReturn(true);

        User actual = target.updateInfo(dto);

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void update_oldPasswordDoesNotMatch_throwsConstraintViolationException() {
        var dto = RandomObject.nextObject(UserInfoDto.class);
        var expected = new ConstraintViolationException(null);

        when(repository.findByUsername(any())).thenReturn(Optional.of(new User()));
        when(securityService.getAuthentication()).thenReturn(Mockito.mock(OAuth2Authentication.class));
        when(securityService.getAuthentication().getName()).thenReturn("admin");
        when(passwordEncoder.matches(any(), any())).thenReturn(false);
        when(constraintExceptionFactory.newConstraintValidationException("oldPassword", "simplefer.validation.password.doesntMatch"))
                .thenReturn(expected);

        ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.updateInfo(dto));

        assertThat(actual).isNotNull().isEqualTo(expected);
    }


    @Test
    public void logout() {
        var excepted = RandomObject.nextObject(String.class);
        when(tokenStore.getAccessToken(any())).thenReturn(mock(OAuth2AccessToken.class));
        when(tokenStore.getAccessToken(any()).getValue()).thenReturn(excepted);

        target.logout();

        verify(tokenServices, times(1)).revokeToken(excepted);
    }
}
