package dev.nicolasbouvie.simplefer.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IBANServiceImplTest {

    @InjectMocks
    private IBANServiceImpl target;

    @Mock
    private RestTemplate restTemplate;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void isValid() throws JsonProcessingException {
        var iban = RandomObject.nextObject(String.class);
        JsonNode expected = objectMapper.readTree("{\"valid\": true}");

        when(restTemplate.getForObject(null, JsonNode.class, Map.of("iban", iban))).thenReturn(expected);

        assertThat(target.isValid(iban)).isTrue();
    }

    @Test
    public void isValid_false() throws JsonProcessingException {
        var iban = RandomObject.nextObject(String.class);
        JsonNode expected = objectMapper.readTree("{\"valid\": false}");

        when(restTemplate.getForObject(null, JsonNode.class, Map.of("iban", iban))).thenReturn(expected);

        assertThat(target.isValid(iban)).isFalse();
    }


    @Test
    public void isValid_whenReturnInvalid_false() throws JsonProcessingException {
        var iban = RandomObject.nextObject(String.class);
        JsonNode expected = objectMapper.readTree("{\"invalid\": false}");

        when(restTemplate.getForObject(null, JsonNode.class, Map.of("iban", iban))).thenReturn(expected);

        assertThat(target.isValid(iban)).isFalse();
    }
}
