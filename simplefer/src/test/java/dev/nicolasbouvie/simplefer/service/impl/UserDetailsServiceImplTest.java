package dev.nicolasbouvie.simplefer.service.impl;


import dev.nicolasbouvie.simplefer.data.model.User;
import dev.nicolasbouvie.simplefer.data.repository.UserRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserDetailsServiceImplTest {

    @InjectMocks
    private UserDetailsServiceImpl target;

    @Mock
    private UserRepository repository;

    @Mock
    private ConstraintExceptionFactory constraintExceptionFactory;

    @Test
    public void loadUserByUsername() {
        var username = RandomObject.nextObject(String.class);
        var user = RandomObject.nextObject(User.class);

        when(repository.findByUsername(username)).thenReturn(Optional.of(user));

        var actual = target.loadUserByUsername(username);

        assertThat(actual).isNotNull();
        assertThat(actual.getUsername()).isEqualTo(user.getUsername());
        assertThat(actual.getPassword()).isEqualTo(user.getPassword());
        assertThat(actual.getAuthorities()).hasSize(1);
        assertThat(actual.getAuthorities().stream().findFirst().get().getAuthority())
                .isEqualTo(user.getRoles().stream().findFirst().get().getName());
    }

    @Test
    public void loadUserByUsername_userNotFound() {
        var username = RandomObject.nextObject(String.class);
        var expected = new EntityNotFoundException("User not found");

        when(repository.findByUsername(username)).thenReturn(Optional.empty());
        when(constraintExceptionFactory.newEntityNotFoundException("User", username)).thenReturn(expected);

        EntityNotFoundException actual = assertThrows(EntityNotFoundException.class, () -> target.loadUserByUsername(username));

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void loadUserByUsername_invalidUsernameFormat() {
        var username = "onlyLettersDigitAndUnderscoreAreAccepted!!!";
        var expected = new ConstraintViolationException(null);

        when(constraintExceptionFactory.newConstraintValidationException("username", "simplefer.validation.username.pattern"))
                .thenReturn(expected);

        ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.loadUserByUsername(username));

        assertThat(actual).isNotNull().isEqualTo(expected);
    }
}
