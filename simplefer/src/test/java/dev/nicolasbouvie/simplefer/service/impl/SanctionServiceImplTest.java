package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.SanctionDto;
import dev.nicolasbouvie.simplefer.data.mapper.SanctionMapper;
import dev.nicolasbouvie.simplefer.data.model.Sanction;
import dev.nicolasbouvie.simplefer.data.repository.SanctionRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SanctionServiceImplTest {

    @InjectMocks
    private SanctionServiceImpl target;

    @Spy
    private SanctionRepository repository;

    @Spy
    private SanctionMapper mapper;

    @Mock
    private ConstraintExceptionFactory constraintExceptionFactory;

    @Test
    public void list_emptyFilter() {
        var filter = new BasePageFilter();

        target.list(filter);

        verify(repository, times(1)).findAll(filter.getPageable("id"));
    }

    @Test
    public void list_nonEmptyFilter() {
        var filter = RandomObject.nextObject(BasePageFilter.class);

        target.list(filter);

        verify(repository, times(1)).findAll(filter.getPageable("shouldUseFromFilter"));
    }

    @Test
    public void get() {
        var id = RandomObject.nextObject(Long.class);
        var expected = RandomObject.nextObject(Sanction.class);

        when(repository.findById(id)).thenReturn(Optional.of(expected));

        Sanction actual = target.get(id);

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void get_notFound_throwsEntityNotFoundException() {
        var id = RandomObject.nextObject(Long.class);
        var expected = new EntityNotFoundException("Sanction not found");

        when(repository.findById(id)).thenReturn(Optional.empty());
        when(constraintExceptionFactory.newEntityNotFoundException("Sanction", id)).thenReturn(expected);

        EntityNotFoundException actual = assertThrows(EntityNotFoundException.class, () -> target.get(id));

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void create() {
        var dto = RandomObject.nextObject(SanctionDto.class);
        dto.setId(null);

        target.create(dto);

        verify(mapper, times(1)).toModel(dto);
        verify(repository, times(1)).save(any());
    }

    @Test
    public void update() {
        var dto = RandomObject.nextObject(SanctionDto.class);
        var expected = RandomObject.nextObject(Sanction.class);

        when(repository.findById(dto.getId())).thenReturn(Optional.of(expected));
        when(mapper.toModel(dto)).thenReturn(expected);
        when(repository.save(any())).thenReturn(expected);

        Sanction actual = target.update(dto);

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void update_notFound_throwsEntityNotFoundException() {
        var dto = RandomObject.nextObject(SanctionDto.class);
        var expected = new EntityNotFoundException("Sanction not found");

        when(repository.findById(dto.getId())).thenReturn(Optional.empty());
        when(constraintExceptionFactory.newEntityNotFoundException("Sanction", dto.getId())).thenReturn(expected);

        EntityNotFoundException actual = assertThrows(EntityNotFoundException.class, () -> target.update(dto));

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void delete() {
        var id = RandomObject.nextObject(Long.class);
        target.delete(id);

        verify(repository, times(1)).deleteById(id);
    }
}
