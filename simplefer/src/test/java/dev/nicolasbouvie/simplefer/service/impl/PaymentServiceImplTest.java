package dev.nicolasbouvie.simplefer.service.impl;


import dev.nicolasbouvie.simplefer.data.dto.PaymentDto;
import dev.nicolasbouvie.simplefer.data.dto.PaymentsFilter;
import dev.nicolasbouvie.simplefer.data.mapper.PaymentMapper;
import dev.nicolasbouvie.simplefer.data.model.Balance;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Currency;
import dev.nicolasbouvie.simplefer.data.model.Payment;
import dev.nicolasbouvie.simplefer.data.repository.BankAccountRepository;
import dev.nicolasbouvie.simplefer.data.repository.CurrencyRepository;
import dev.nicolasbouvie.simplefer.data.repository.PaymentRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import dev.nicolasbouvie.simplefer.service.BalanceService;
import dev.nicolasbouvie.simplefer.service.FraudCheckService;
import dev.nicolasbouvie.simplefer.service.IBANService;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PaymentServiceImplTest {

    @InjectMocks
    @Spy
    private PaymentServiceImpl target;

    @Mock private PaymentMapper mapper;
    @Mock private PaymentRepository repository;
    @Mock private BalanceService balanceService;
    @Mock private BankAccountRepository bankAccountRepository;
    @Mock private FraudCheckService fraudCheckService;
    @Mock private IBANService ibanService;
    @Mock private UserServiceImpl userService;
    @Mock private CurrencyRepository currencyRepository;
    @Mock private ConstraintExceptionFactory constraintExceptionFactory;

    @Test
    public void list_emptyFilter() {
        var filter = new PaymentsFilter();

        target.list(filter);

        verify(repository, times(1))
                .findAllByUsernameAndCreationDateBetween(userService.getCurrentUsername(),
                    filter.getStartDate().atStartOfDay(), filter.getEndDate().plusDays(1).atStartOfDay(),
                    filter.getPageable("creationDate"));
    }

    @Test
    public void list_nonEmptyFilter() {
        var filter = RandomObject.nextObject(PaymentsFilter.class);

        target.list(filter);

        verify(repository, times(1))
                .findAllByUsernameAndCreationDateBetween(userService.getCurrentUsername(),
                        filter.getStartDate().atStartOfDay(), filter.getEndDate().plusDays(1).atStartOfDay(),
                        filter.getPageable("shouldUseFromFilter"));
    }

    @Test
    public void listByBeneficiary_emptyFilter() {
        var iban = RandomObject.nextObject(String.class);
        var filter = new PaymentsFilter();

        when(ibanService.isValid(iban)).thenReturn(true);

        target.listByBeneficiary(iban, filter);

        verify(repository, times(1))
                .findAllToAccount(userService.getCurrentUsername(), iban,
                        filter.getStartDate().atStartOfDay(), filter.getEndDate().plusDays(1).atStartOfDay(),
                        filter.getPageable("creationDate"));
    }

    @Test
    public void listByBeneficiary_invalidIban() {
        var iban = RandomObject.nextObject(String.class);
        var filter = RandomObject.nextObject(PaymentsFilter.class);
        var expected = new ConstraintViolationException(null);

        when(ibanService.isValid(iban)).thenReturn(false);
        when(constraintExceptionFactory.newConstraintValidationException("accountNumber", "simplefer.validation.iban"))
                .thenReturn(expected);

        ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.listByBeneficiary(iban, filter));

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void listByBeneficiary_nonEmptyFilter() {
        var iban = RandomObject.nextObject(String.class);
        var filter = RandomObject.nextObject(PaymentsFilter.class);

        when(ibanService.isValid(iban)).thenReturn(true);

        target.listByBeneficiary(iban, filter);

        verify(repository, times(1))
                .findAllToAccount(userService.getCurrentUsername(), iban,
                        filter.getStartDate().atStartOfDay(), filter.getEndDate().plusDays(1).atStartOfDay(),
                        filter.getPageable("shouldUseFromFilter"));
    }

    @Test
    public void create_giverAccountDoesNotBelongToAuthUser_throwsConstraintViolationException() {
        var dto = RandomObject.nextObject(PaymentDto.class);
        dto.setId(null);
        dto.setType(null);
        dto.setStatus(null);
        var expected = new ConstraintViolationException(null);

        when(mapper.toModel(dto)).thenReturn(Mappers.getMapper(PaymentMapper.class).toModel(dto));
        when(bankAccountRepository.findByAccountNumberAndUsers_Username(dto.getGiverAccountNumber(), userService.getCurrentUsername()))
                .thenReturn(Optional.empty());
        when(constraintExceptionFactory.newConstraintValidationException("giverAccountNumber", "simplefer.validation.giverAccount.notFound"))
                .thenReturn(expected);

        ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.create(dto));
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void create_currencyNotSupported_throwsConstraintViolationException() {
        var dto = RandomObject.nextObject(PaymentDto.class);
        dto.setId(null);
        dto.setType(null);
        dto.setStatus(null);
        var expected = new ConstraintViolationException(null);

        when(mapper.toModel(dto)).thenReturn(Mappers.getMapper(PaymentMapper.class).toModel(dto));
        when(bankAccountRepository.findByAccountNumberAndUsers_Username(dto.getGiverAccountNumber(), userService.getCurrentUsername()))
                .thenReturn(Optional.of(RandomObject.nextObject(BankAccount.class)));
        when(currencyRepository.findByCode(dto.getCurrency()))
                .thenReturn(Optional.empty());
        when(constraintExceptionFactory.newConstraintValidationException("currency", "simplefer.validation.currency.notFound"))
                .thenReturn(expected);

        ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.create(dto));
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void create_sameAccount_throwsConstraintViolationException() {
        var dto = RandomObject.nextObject(PaymentDto.class);
        dto.setId(null);
        dto.setType(null);
        dto.setStatus(null);
        var giverAccount = RandomObject.nextObject(BankAccount.class);
        dto.setBeneficiaryAccountNumber(giverAccount.getAccountNumber());
        var payment = Mappers.getMapper(PaymentMapper.class).toModel(dto);
        var expected = new ConstraintViolationException(null);

        when(mapper.toModel(dto)).thenReturn(payment);
        when(bankAccountRepository.findByAccountNumberAndUsers_Username(dto.getGiverAccountNumber(), userService.getCurrentUsername()))
                .thenReturn(Optional.of(giverAccount));
        when(currencyRepository.findByCode(dto.getCurrency()))
                .thenReturn(Optional.of(RandomObject.nextObject(Currency.class)));
        when(constraintExceptionFactory.newConstraintValidationException("beneficiaryAccountNumber", "simplefer.validation.giverAccount.equalsBeneficiary"))
                .thenReturn(expected);

        ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.create(dto));
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void create_blockedAccount_throwsConstraintViolationException() {
        var dto = RandomObject.nextObject(PaymentDto.class);
        dto.setId(null);
        dto.setType(null);
        dto.setStatus(null);
        var giverAccount = RandomObject.nextObject(BankAccount.class);
        giverAccount.setStatus(BankAccount.Status.BLOCKED);
        var payment = Mappers.getMapper(PaymentMapper.class).toModel(dto);
        var expected = new ConstraintViolationException(null);

        when(mapper.toModel(dto)).thenReturn(payment);
        when(bankAccountRepository.findByAccountNumberAndUsers_Username(dto.getGiverAccountNumber(), userService.getCurrentUsername()))
                .thenReturn(Optional.of(giverAccount));
        when(currencyRepository.findByCode(dto.getCurrency()))
                .thenReturn(Optional.of(RandomObject.nextObject(Currency.class)));
        when(constraintExceptionFactory.newConstraintValidationException("giverAccountNumber", "simplefer.validation.giverAccount.blocked"))
                .thenReturn(expected);

        ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.create(dto));
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void create_futurePayment() {
        var dto = RandomObject.nextObject(PaymentDto.class);
        dto.setId(null);
        dto.setType(null);
        dto.setStatus(null);
        dto.setExecutionDate(LocalDate.now().plusDays(1));
        var giverAccount = RandomObject.nextObject(BankAccount.class);
        giverAccount.setStatus(BankAccount.Status.ENABLE);
        var payment = Mappers.getMapper(PaymentMapper.class).toModel(dto);

        when(mapper.toModel(dto)).thenReturn(payment);
        when(bankAccountRepository.findByAccountNumberAndUsers_Username(dto.getGiverAccountNumber(), userService.getCurrentUsername()))
                .thenReturn(Optional.of(giverAccount));
        when(currencyRepository.findByCode(dto.getCurrency()))
                .thenReturn(Optional.of(RandomObject.nextObject(Currency.class)));
        when(repository.save(any())).thenReturn(payment);

        Payment actual = target.create(dto);

        assertThat(actual).isNotNull();
        assertThat(actual.getStatus()).isEqualTo(Payment.Status.SCHEDULED);
    }

    @Test
    public void create_insufficientFunds() {
        var dto = RandomObject.nextObject(PaymentDto.class);
        dto.setId(null);
        dto.setType(null);
        dto.setStatus(null);
        dto.setAmount(20);
        dto.setExecutionDate(LocalDate.now());
        var giverAccount = RandomObject.nextObject(BankAccount.class);
        giverAccount.setStatus(BankAccount.Status.ENABLE);
        var balance = new Balance();
        balance.setAmount(BigDecimal.TEN);
        var payment = Mappers.getMapper(PaymentMapper.class).toModel(dto);
        var expected = new ConstraintViolationException(null);

        when(mapper.toModel(dto)).thenReturn(payment);
        when(bankAccountRepository.findByAccountNumberAndUsers_Username(dto.getGiverAccountNumber(), userService.getCurrentUsername()))
                .thenReturn(Optional.of(giverAccount));
        when(currencyRepository.findByCode(dto.getCurrency()))
                .thenReturn(Optional.of(RandomObject.nextObject(Currency.class)));
        when(balanceService.getAvailableBalance(any(), any()))
                .thenReturn(balance);
        when(repository.save(any())).thenReturn(payment);
        when(constraintExceptionFactory.newConstraintValidationException("amount", "simplefer.validation.giverAccount.insufficientFunds"))
                .thenReturn(expected);

        ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.create(dto));

        assertThat(actual).isEqualTo(expected);
        assertThat(payment.getStatus()).isEqualTo(Payment.Status.REJECTED);
    }

    @Test
    public void create_notOnUs() {
        var dto = RandomObject.nextObject(PaymentDto.class);
        dto.setId(null);
        dto.setType(null);
        dto.setStatus(null);
        dto.setAmount(5);
        dto.setExecutionDate(LocalDate.now());
        var giverAccount = RandomObject.nextObject(BankAccount.class);
        giverAccount.setStatus(BankAccount.Status.ENABLE);
        var balance = new Balance();
        balance.setAmount(BigDecimal.TEN);
        var payment = Mappers.getMapper(PaymentMapper.class).toModel(dto);

        when(mapper.toModel(dto)).thenReturn(payment);
        when(bankAccountRepository.findByAccountNumberAndUsers_Username(dto.getGiverAccountNumber(), userService.getCurrentUsername()))
                .thenReturn(Optional.of(giverAccount));
        when(currencyRepository.findByCode(dto.getCurrency()))
                .thenReturn(Optional.of(RandomObject.nextObject(Currency.class)));
        when(balanceService.getAvailableBalance(any(), any()))
                .thenReturn(balance);
        when(repository.save(any())).thenReturn(payment);

        var actual = target.create(dto);

        assertThat(actual.getStatus()).isEqualTo(Payment.Status.EXECUTED);
        verify(balanceService, times(1)).save(any());
    }


    @Test
    public void create_onUs() {
        var dto = RandomObject.nextObject(PaymentDto.class);
        dto.setId(null);
        dto.setType(null);
        dto.setStatus(null);
        dto.setAmount(5);
        dto.setExecutionDate(LocalDate.now());
        var giverAccount = RandomObject.nextObject(BankAccount.class);
        giverAccount.setStatus(BankAccount.Status.ENABLE);
        var balance = new Balance();
        balance.setAmount(BigDecimal.TEN);
        var payment = Mappers.getMapper(PaymentMapper.class).toModel(dto);

        when(mapper.toModel(dto)).thenReturn(payment);
        when(bankAccountRepository.findByAccountNumberAndUsers_Username(dto.getGiverAccountNumber(), userService.getCurrentUsername()))
                .thenReturn(Optional.of(giverAccount));
        when(currencyRepository.findByCode(dto.getCurrency()))
                .thenReturn(Optional.of(RandomObject.nextObject(Currency.class)));
        when(balanceService.getAvailableBalance(any(), any()))
                .thenReturn(balance);
        when(bankAccountRepository.findByAccountNumber(any()))
                .thenReturn(Optional.of(giverAccount));
        when(repository.save(any())).thenReturn(payment);

        var actual = target.create(dto);

        assertThat(actual.getStatus()).isEqualTo(Payment.Status.EXECUTED);
        verify(balanceService, times(2)).save(any());
    }

    @Test
    public void returnPayment_notFound() {
        var id = RandomObject.nextObject(Long.class);
        var expected = new EntityNotFoundException("Payment not found");

        when(repository.findReceivedByIdAndUsername(id, userService.getCurrentUsername()))
                .thenReturn(Optional.empty());
        when(constraintExceptionFactory.newEntityNotFoundException("Payment", id))
                .thenReturn(expected);

        EntityNotFoundException actual = assertThrows(EntityNotFoundException.class, () -> target.returnPayment(id));

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void returnPayment() {
        var dto = RandomObject.nextObject(PaymentDto.class);
        dto.setType(Payment.Type.PAYMENT.name());
        dto.setStatus(Payment.Status.EXECUTED.name());
        dto.setAmount(5);
        dto.setExecutionDate(LocalDate.now());
        var giverAccount = RandomObject.nextObject(BankAccount.class);
        giverAccount.setStatus(BankAccount.Status.ENABLE);
        var balance = new Balance();
        balance.setAmount(BigDecimal.TEN);
        var payment = Mappers.getMapper(PaymentMapper.class).toModel(dto);

        when(repository.findReceivedByIdAndUsername(dto.getId(), userService.getCurrentUsername()))
                .thenReturn(Optional.of(payment));
        when(mapper.toModel(dto)).thenReturn(payment);
        when(mapper.toDto(payment)).thenReturn(dto);
        when(bankAccountRepository.findByAccountNumberAndUsers_Username(any(), any()))
                .thenReturn(Optional.of(giverAccount));
        when(currencyRepository.findByCode(any()))
                .thenReturn(Optional.of(RandomObject.nextObject(Currency.class)));
        when(balanceService.getAvailableBalance(any(), any()))
                .thenReturn(balance);
        when(bankAccountRepository.findByAccountNumber(any()))
                .thenReturn(Optional.of(giverAccount));
        when(repository.save(any())).thenReturn(payment);

        target.returnPayment(dto.getId());

        assertThat(dto.getStatus()).isEqualTo(Payment.Status.EXECUTED.name());
        assertThat(dto.getType()).isEqualTo(Payment.Type.RETURN.name());
        verify(balanceService, times(2)).save(any());
    }

    @Test
    public void deleteScheduled_paymentNotFound() {
        var id = RandomObject.nextObject(Long.class);
        var expected = new EntityNotFoundException("Payment not found");

        when(repository.findByIdAndUsername(id, userService.getCurrentUsername()))
                .thenReturn(Optional.empty());
        when(constraintExceptionFactory.newEntityNotFoundException("Payment", id))
                .thenReturn(expected);

        EntityNotFoundException actual = assertThrows(EntityNotFoundException.class, () -> target.deleteScheduled(id));

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void deleteScheduled_paymentIsNotScheduled() {
        var id = RandomObject.nextObject(Long.class);
        var payment = RandomObject.nextObject(Payment.class);
        var expected = new ConstraintViolationException(null);

        when(repository.findByIdAndUsername(id, userService.getCurrentUsername()))
                .thenReturn(Optional.of(payment));

        for (Payment.Status status : Payment.Status.values()) {
            if (status == Payment.Status.SCHEDULED) continue;
            payment.setStatus(status);
            when(constraintExceptionFactory.newConstraintValidationException(
                    "status", "simplefer.validation.payment.statusNotScheduled", payment.getStatus()))
                    .thenReturn(expected);

            ConstraintViolationException actual = assertThrows(ConstraintViolationException.class, () -> target.deleteScheduled(id));

            assertThat(actual).isEqualTo(expected);
        }
    }


    @Test
    public void deleteScheduled() {
        var id = RandomObject.nextObject(Long.class);
        var payment = RandomObject.nextObject(Payment.class);
        payment.setStatus(Payment.Status.SCHEDULED);

        when(repository.findByIdAndUsername(id, userService.getCurrentUsername()))
                .thenReturn(Optional.of(payment));

        target.deleteScheduled(id);

        verify(repository, times(1)).delete(payment);
    }

    @Test
    public void executeScheduledPayment_accountBlocked() {
        var payment = spy(RandomObject.nextObject(Payment.class));
        payment.getGiverAccount().setStatus(BankAccount.Status.BLOCKED);

        target.executeScheduledPayment(payment);

        verify(payment).setStatus(Payment.Status.REJECTED);
        verify(repository).save(payment);
    }

    @Test
    public void executeScheduledPayment() {
        var payment = spy(RandomObject.nextObject(Payment.class));
        var balance = new Balance();
        balance.setAmount(BigDecimal.TEN);
        payment.getGiverAccount().setStatus(BankAccount.Status.ENABLE);
        when(balanceService.getAvailableBalance(any(), any()))
                .thenReturn(balance);

        target.executeScheduledPayment(payment);

        verify(fraudCheckService).checkPayment(payment);
        verify(target).executePayment(payment);
        verify(repository).save(payment);
    }
}
