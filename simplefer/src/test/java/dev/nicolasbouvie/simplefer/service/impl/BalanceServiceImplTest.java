package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.model.Balance;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Currency;
import dev.nicolasbouvie.simplefer.data.repository.BalanceRepository;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class BalanceServiceImplTest {

    @InjectMocks
    private BalanceServiceImpl target;

    @Spy
    private BalanceRepository repository;

    @Test
    public void getAvailableBalance_nonExistentCurrency_saveNew() {
        var currency = RandomObject.nextObject(Currency.class);
        var account = RandomObject.nextObject(BankAccount.class);

        Mockito.when(repository.findBalanceByCurrencyAndBankAccountAndType(currency, account, Balance.Type.AVAILABLE))
                .thenReturn(Optional.empty());

        target.getAvailableBalance(currency, account);

        Mockito.verify(repository, Mockito.times(1)).save(Mockito.any());

    }

    @Test
    public void getAvailableBalance_existentCurrentCurrency_return() {
        var currency = RandomObject.nextObject(Currency.class);
        var account = RandomObject.nextObject(BankAccount.class);
        var balance = RandomObject.nextObject(Balance.class);
        balance.setDate(LocalDate.now());

        Mockito.when(repository.findBalanceByCurrencyAndBankAccountAndType(currency, account, Balance.Type.AVAILABLE))
                .thenReturn(Optional.of(balance));

        Balance availableBalance = target.getAvailableBalance(currency, account);

        assertThat(availableBalance).isNotNull().isEqualTo(balance);

        Mockito.verify(repository, Mockito.never()).save(Mockito.any());
    }

    @Test
    public void getAvailableBalance_existentPastBalance_saveAsEODAndReturnNewWithSameAmount() {
        var currency = RandomObject.nextObject(Currency.class);
        var account = RandomObject.nextObject(BankAccount.class);
        var balance = RandomObject.nextObject(Balance.class);
        balance.setDate(LocalDate.now().minusDays(1));
        balance.setType(Balance.Type.AVAILABLE);

        Mockito.when(repository.findBalanceByCurrencyAndBankAccountAndType(currency, account, Balance.Type.AVAILABLE))
                .thenReturn(Optional.of(balance));

        target.getAvailableBalance(currency, account);

        Mockito.verify(repository, Mockito.times(2)).save(Mockito.any());
    }

    @Test
    public void save() {
        var balance = RandomObject.nextObject(Balance.class);
        target.save(balance);
        Mockito.verify(repository, Mockito.times(1)).save(balance);
    }
}
