package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.repository.BankAccountRepository;
import dev.nicolasbouvie.simplefer.service.UserService;
import dev.nicolasbouvie.simplefer.test.util.RandomObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class BankAccountServiceImplTest {

    @InjectMocks
    private BankAccountServiceImpl target;

    @Mock
    private BankAccountRepository repository;

    @Mock
    private UserService userService;

    @Test
    public void list_emptyFilter() {
        var username = RandomObject.nextObject(String.class);
        var filter = new BasePageFilter();
        var expected = new PageImpl<>(RandomObject.objects(BankAccount.class, 2).collect(Collectors.toList()));

        Mockito.when(userService.getCurrentUsername()).thenReturn(username);
        Mockito.when(repository.findAllByUsers_Username(username, filter.getPageable("accountName")))
            .thenReturn(expected);

        Page<BankAccount> actual = target.list(filter);

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void list_nonEmptyFilter() {
        var username = RandomObject.nextObject(String.class);
        var filter = RandomObject.nextObject(BasePageFilter.class);
        var expected = new PageImpl<>(RandomObject.objects(BankAccount.class, 2).collect(Collectors.toList()));

        Mockito.when(userService.getCurrentUsername()).thenReturn(username);
        Mockito.when(repository.findAllByUsers_Username(username, filter.getPageable("shouldUseFromFilter")))
            .thenReturn(expected);

        Page<BankAccount> actual = target.list(filter);

        assertThat(actual).isNotNull().isEqualTo(expected);
    }
    @Test
    public void listByUser_emptyFilter() {
        var username = RandomObject.nextObject(String.class);
        var filter = new BasePageFilter();
        var expected = new PageImpl<>(RandomObject.objects(BankAccount.class, 2).collect(Collectors.toList()));

        Mockito.when(repository.findAllByUsers_Username(username, filter.getPageable("accountName")))
            .thenReturn(expected);

        Page<BankAccount> actual = target.listByUser(username, filter);

        assertThat(actual).isNotNull().isEqualTo(expected);
    }

    @Test
    public void listByUser_nonEmptyFilter() {
        var username = RandomObject.nextObject(String.class);
        var filter = RandomObject.nextObject(BasePageFilter.class);
        var expected = new PageImpl<>(RandomObject.objects(BankAccount.class, 2).collect(Collectors.toList()));

        Mockito.when(repository.findAllByUsers_Username(username, filter.getPageable("shouldUseFromFilter")))
            .thenReturn(expected);

        Page<BankAccount> actual = target.listByUser(username, filter);

        assertThat(actual).isNotNull().isEqualTo(expected);
    }
}
