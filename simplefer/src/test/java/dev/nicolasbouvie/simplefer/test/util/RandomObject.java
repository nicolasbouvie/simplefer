package dev.nicolasbouvie.simplefer.test.util;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;

import java.time.LocalDate;
import java.util.Random;
import java.util.stream.Stream;

/**
 * Utility to generate random objects
 */
public class RandomObject {
    private static final RandomObject INSTANCE = new RandomObject();
    private final EasyRandom easyRandom;

    private RandomObject() {
        var parameters = new EasyRandomParameters();
        parameters.stringLengthRange(3, 3);
        parameters.collectionSizeRange(1, 1);
        parameters.randomizationDepth(2);
        parameters.dateRange(LocalDate.now().minusYears(10), LocalDate.now());
        parameters.randomize(Integer.TYPE, ()->Math.abs(new Random().nextInt(Integer.MAX_VALUE-1)+1));
        this.easyRandom = new EasyRandom(parameters);
    }

    /**
     * Generate a random object.
     *
     * @param type Class of type to generate
     * @param <T> Type
     * @return Random object
     */
    public static <T> T nextObject(Class<T> type) {
        return INSTANCE.easyRandom.nextObject(type);
    }

    /**
     * Generate a stream of random object of specific type
     * @param type Class of type to generate
     * @param size Size of stream
     * @param <T> Type
     * @return Stream of objects
     */
    public static <T> Stream<T> objects(Class<T> type, int size) {
        return INSTANCE.easyRandom.objects(type, size);
    }
}
