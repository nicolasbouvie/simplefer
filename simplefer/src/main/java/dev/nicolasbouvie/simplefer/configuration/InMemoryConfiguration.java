package dev.nicolasbouvie.simplefer.configuration;

import dev.nicolasbouvie.simplefer.data.model.Balance;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Currency;
import dev.nicolasbouvie.simplefer.data.model.Role;
import dev.nicolasbouvie.simplefer.data.model.Sanction;
import dev.nicolasbouvie.simplefer.data.model.User;
import dev.nicolasbouvie.simplefer.data.repository.BalanceRepository;
import dev.nicolasbouvie.simplefer.data.repository.BankAccountRepository;
import dev.nicolasbouvie.simplefer.data.repository.CurrencyRepository;
import dev.nicolasbouvie.simplefer.data.repository.RoleRepository;
import dev.nicolasbouvie.simplefer.data.repository.SanctionRepository;
import dev.nicolasbouvie.simplefer.data.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableAuthorizationServer
@Profile("default")
public class InMemoryConfiguration {

    @Bean
    public TokenStore tokenStore() {
        return new InMemoryTokenStore();
    }

    @Bean
    public CommandLineRunner loadData(UserRepository userRepository,
                                      RoleRepository roleRepository,
                                      BalanceRepository balanceRepository,
                                      BankAccountRepository accountRepository,
                                      CurrencyRepository currencyRepository,
                                      SanctionRepository sanctionRepository,
                                      PasswordEncoder passwordEncoder) {
        return (args) -> {
            var currencies = createSamples(currencyRepository);
            var roles = createSamples(roleRepository);
            var users = createSamples(userRepository, passwordEncoder, roles);
            var accounts = createSamples(accountRepository, users);
            var balances = createSamples(balanceRepository, currencies, accounts);
            var sanctions = createSamples(sanctionRepository);
        };
    }

    private List<Role> createSamples(RoleRepository roleRepository) {
        var roles = new ArrayList<Role>();
        {
            var role = new Role();
            role.setName("ADMIN");
            roles.add(roleRepository.save(role));
        }
        {
            var role = new Role();
            role.setName("USER");
            roles.add(roleRepository.save(role));
        }
        return roles;
    }

    private List<Currency> createSamples(CurrencyRepository currencyRepository) {
        var currencies = new ArrayList<Currency>();
        {
            var currency = new Currency();
            currency.setCode("PLN");
            currencies.add(currencyRepository.save(currency));
        }
        {
            var currency = new Currency();
            currency.setCode("EUR");
            currencies.add(currencyRepository.save(currency));
        }
        return currencies;
    }

    private List<User> createSamples(UserRepository userRepository, PasswordEncoder passwordEncoder, List<Role> roles) {
        var users = new ArrayList<User>();
        {
            var user = new User();
            user.setUsername("admin");
            user.setPassword(passwordEncoder.encode("password"));
            user.setAddress("admin address");
            user.setRoles(List.of(roles.get(0)));
            users.add(userRepository.save(user));
        }
        {
            var user = new User();
            user.setUsername("user1");
            user.setPassword(passwordEncoder.encode("password"));
            user.setAddress("user1 address");
            user.setRoles(List.of(roles.get(1)));
            users.add(userRepository.save(user));
        }
        {
            var user = new User();
            user.setUsername("user2");
            user.setPassword(passwordEncoder.encode("password"));
            user.setAddress("user2 address");
            user.setRoles(List.of(roles.get(1)));
            users.add(userRepository.save(user));
        }
        return users;
    }

    private List<BankAccount> createSamples(BankAccountRepository accountRepository,
                                            List<User> users) {
        var accounts = new ArrayList<BankAccount>();
        {
            var account = new BankAccount();
            account.setAccountNumber("PL61109010140000071219812874");
            account.setAccountName("Account 1");
            account.setUsers(List.of(users.get(0)));
            accounts.add(accountRepository.save(account));
        }
        {
            var account = new BankAccount();
            account.setAccountNumber("DE75512108001245126199");
            account.setAccountName("Account 2");
            account.setUsers(List.of(users.get(0)));
            accounts.add(accountRepository.save(account));
        }
        {
            var account = new BankAccount();
            account.setAccountNumber("LU120010001234567891");
            account.setAccountName("Account 3");
            account.setStatus(BankAccount.Status.BLOCKED);
            account.setUsers(List.of(users.get(1)));
            accounts.add(accountRepository.save(account));
        }
        {
            var account = new BankAccount();
            account.setAccountNumber("PT50000201231234567890154");
            account.setAccountName("Account 4");
            account.setUsers(List.of(users.get(1)));
            accounts.add(accountRepository.save(account));
        }
        {
            var account = new BankAccount();
            account.setAccountNumber("IT60X0542811101000000123456");
            account.setAccountName("Account 5");
            account.setUsers(List.of(users.get(1), users.get(2)));
            accounts.add(accountRepository.save(account));
        }
        {
            var account = new BankAccount();
            account.setAccountNumber("ES9121000418450200051332");
            account.setAccountName("Account 6");
            account.setUsers(List.of(users.get(2)));
            accounts.add(accountRepository.save(account));
        }
        return accounts;
    }

    private List<Balance> createSamples(BalanceRepository balanceRepository,
                                        List<Currency> currencies,
                                        List<BankAccount> accounts) {
        var balances = new ArrayList<Balance>();
        {
            var balance = new Balance();
            balance.setBankAccount(accounts.get(0));
            balance.setAmount(BigDecimal.valueOf(10000));
            balance.setCurrency(currencies.get(0));
            balanceRepository.save(balance);
        }
        {
            var balance = new Balance();
            balance.setBankAccount(accounts.get(0));
            balance.setAmount(BigDecimal.valueOf(100));
            balance.setCurrency(currencies.get(1));
            balanceRepository.save(balance);
        }
        {
            var balance = new Balance();
            balance.setBankAccount(accounts.get(1));
            balance.setAmount(BigDecimal.valueOf(3000));
            balance.setCurrency(currencies.get(1));
            balanceRepository.save(balance);
        }
        {
            var balance = new Balance();
            balance.setBankAccount(accounts.get(2));
            balance.setAmount(BigDecimal.valueOf(500000));
            balance.setCurrency(currencies.get(1));
            balance.setDate(LocalDate.now().minusDays(10));
            balanceRepository.save(balance);
        }
        {
            var balance = new Balance();
            balance.setBankAccount(accounts.get(3));
            balance.setAmount(BigDecimal.valueOf(5786));
            balance.setCurrency(currencies.get(1));
            balance.setDate(LocalDate.now().minusDays(5));
            balanceRepository.save(balance);
        }
        {
            var balance = new Balance();
            balance.setBankAccount(accounts.get(4));
            balance.setAmount(BigDecimal.valueOf(1234));
            balance.setCurrency(currencies.get(1));
            balanceRepository.save(balance);
        }
        {
            var balance = new Balance();
            balance.setBankAccount(accounts.get(5));
            balance.setAmount(BigDecimal.valueOf(987));
            balance.setCurrency(currencies.get(1));
            balanceRepository.save(balance);
        }
        return balances;
    }

    private List<Sanction> createSamples(SanctionRepository sanctionRepository) {
        var sanctions = new ArrayList<Sanction>();
        {
            var sanction = new Sanction();
            sanction.setAccountNumber("LU280019400644750000");
            sanction.setReason("simplefer.validation.sanction");
            sanctions.add(sanctionRepository.save(sanction));
        }
        {
            var sanction = new Sanction();
            sanction.setAccountNumber("LU120010001234567891");
            sanction.setReason("simplefer.validation.sanction");
            sanctions.add(sanctionRepository.save(sanction));
        }
        return sanctions;
    }
}
