package dev.nicolasbouvie.simplefer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SimpleferApplication {
    public static void main(String[] args) {
        SpringApplication.run(SimpleferApplication.class, args);
    }
}
