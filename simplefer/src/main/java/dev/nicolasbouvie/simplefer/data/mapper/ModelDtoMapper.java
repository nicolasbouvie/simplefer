package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.Dto;
import dev.nicolasbouvie.simplefer.data.model.Model;

/**
 * Mapstruct interface to map from/to Model and Dto.
 *
 * @param <MODEL> Model type
 * @param <DTO>   Dto type
 */
public interface ModelDtoMapper<MODEL extends Model, DTO extends Dto> {

    /**
     * Map Dto to Model
     *
     * @param dto Source object
     * @return Mapped Model
     */
    MODEL toModel(DTO dto);

    /**
     * Map Model to Dto
     *
     * @param model Source object
     * @return Mapped Dto
     */
    DTO toDto(MODEL model);
}
