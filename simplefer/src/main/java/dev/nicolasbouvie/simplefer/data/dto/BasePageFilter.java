package dev.nicolasbouvie.simplefer.data.dto;

import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

@Data
public class BasePageFilter {
    private int page = 0;
    private int pageSize = 20;
    private List<String> sortFields;
    private String sortDirection;

    /**
     * Build a Pageable object from current properties
     *
     * @param defSort Default sort fields to use in case {@link BasePageFilter#sortFields} is empty
     * @return Pageable object
     */
    public Pageable getPageable(String... defSort) {
        Sort.Direction sortDirection = Sort.Direction.fromOptionalString(this.sortDirection).orElse(Sort.Direction.ASC);
        String[] sortFields = this.sortFields == null || this.sortFields.isEmpty() ? defSort : this.sortFields.toArray(new String[0]);
        return PageRequest.of(page, pageSize, Sort.by(sortDirection, sortFields));
    }
}
