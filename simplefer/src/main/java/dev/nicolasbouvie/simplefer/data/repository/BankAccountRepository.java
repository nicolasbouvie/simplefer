package dev.nicolasbouvie.simplefer.data.repository;

import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BankAccountRepository extends PagingAndSortingRepository<BankAccount, Long> {
    Page<BankAccount> findAllByUsers_Username(String username, Pageable pageable);

    Optional<BankAccount> findByAccountNumberAndUsers_Username(String accountNumber, String username);

    Optional<BankAccount> findByAccountNumber(String accountNumber);
}
