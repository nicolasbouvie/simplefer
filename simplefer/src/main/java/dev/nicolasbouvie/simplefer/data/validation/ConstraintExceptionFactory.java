package dev.nicolasbouvie.simplefer.data.validation;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.util.Locale;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ConstraintExceptionFactory {
    private final MessageSource messages;

    /**
     * Create a new {@link ConstraintViolationException} with specific path and messageKey from ValidationMessages
     * @param path Path to identify the root of the exception
     * @param messageKey Key to lookup
     * @param params Message parameters
     * @return ConstraintViolationException
     */
    public ConstraintViolationException newConstraintValidationException(String path, String messageKey, Object... params) {
        var message = messages.getMessage(messageKey, params, Locale.getDefault());
        var constraint = new CustomConstraintViolation(message, path);
        return new ConstraintViolationException(Set.of(constraint));
    }

    /**
     * Create a new {@link EntityNotFoundException} with specific message from ValidationMessages
     * @param entity Name of entity not found
     * @param id Entity id
     * @return EntityNotFoundException
     */
    public EntityNotFoundException newEntityNotFoundException(String entity, Object id) {
        var message = messages.getMessage("simplefer.validation.entity.notFound", new Object[]{entity, id}, Locale.getDefault());
        return new EntityNotFoundException(message);
    }
}
