package dev.nicolasbouvie.simplefer.data.repository;

import dev.nicolasbouvie.simplefer.data.model.Currency;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrencyRepository extends PagingAndSortingRepository<Currency, Long> {
    Optional<Currency> findByCode(String code);
}
