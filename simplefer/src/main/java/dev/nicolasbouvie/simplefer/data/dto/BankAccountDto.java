package dev.nicolasbouvie.simplefer.data.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

@Data
@EqualsAndHashCode(of = {"accountNumber"})
public class BankAccountDto implements Dto {
    private Long id;
    private Set<String> users;
    private String accountNumber;
    private String accountName;
    private String status;
}
