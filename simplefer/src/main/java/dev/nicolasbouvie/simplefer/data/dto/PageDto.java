package dev.nicolasbouvie.simplefer.data.dto;

import lombok.Data;

import java.util.Collection;

@Data
public class PageDto<T extends Dto> implements Dto {
    private int totalPages;
    private int page;
    private int pageSize;
    private long totalElements;
    private int numberOfElements;
    private Collection<T> content;
}
