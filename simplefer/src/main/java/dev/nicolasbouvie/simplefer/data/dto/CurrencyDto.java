package dev.nicolasbouvie.simplefer.data.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@EqualsAndHashCode(of = {"code"})
public class CurrencyDto implements Dto {

    private Long id;

    @NotBlank(message = "{simplefer.validation.currency.required}")
    @Size(min=3, max=3, message = "{simplefer.validation.currency.size}")
    private String code;
}
