package dev.nicolasbouvie.simplefer.data.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(
    name = "BALANCE",
    uniqueConstraints = {
        @UniqueConstraint(
            name = "BALANCE_UK",
            columnNames = {"BANK_ACCOUNT_ID", "CURRENCY_ID", "BALANCE_DATE", "TYPE"}
        )
    }
)
@Data
public class Balance implements Model {

    @Id
    @SequenceGenerator(name = "BALANCE_ID_SEQ", sequenceName = "BALANCE_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BALANCE_ID_SEQ")
    @Column(name = "ID", unique = true)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "BANK_ACCOUNT_ID", nullable = false)
    @NotNull(message = "Bank account is mandatory")
    private BankAccount bankAccount;

    @Column(name = "AMOUNT", nullable = false)
    @NotNull(message = "Amount is mandatory")
    private BigDecimal amount = BigDecimal.ZERO;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CURRENCY_ID", nullable = false)
    @NotNull(message = "Currency is mandatory")
    private Currency currency;

    @Column(name = "BALANCE_DATE", nullable = false, columnDefinition = "DATE")
    @NotNull(message = "Date is mandatory")
    private LocalDate date = LocalDate.now();

    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull(message = "Type is mandatory")
    private Type type = Type.AVAILABLE;

    public enum Type {
        END_OF_DAY, AVAILABLE
    }
}
