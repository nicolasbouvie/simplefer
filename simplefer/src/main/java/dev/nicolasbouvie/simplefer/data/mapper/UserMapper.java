package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.RoleDto;
import dev.nicolasbouvie.simplefer.data.dto.UserDto;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Role;
import dev.nicolasbouvie.simplefer.data.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RoleMapper.class, BankAccountMapper.class})
public interface UserMapper extends ModelDtoMapper<User, UserDto> {

    @Override
    @Mapping(target = "password", ignore = true)
    UserDto toDto(User model);

    default Role roleToModel(String name) {
        var role = new Role();
        role.setName(name);
        return role;
    }

    default String roleToString(Role role) {
        return role != null ? role.getName() : null;
    }

    default BankAccount accountToModel(String accountNumber) {
        var account = new BankAccount();
        account.setAccountNumber(accountNumber);
        return account;
    }

    default String accountToString(BankAccount account) {
        return account != null ? account.getAccountNumber() : null;
    }
}
