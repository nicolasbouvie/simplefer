package dev.nicolasbouvie.simplefer.data.validation;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.metadata.ConstraintDescriptor;
import java.util.Iterator;

@EqualsAndHashCode(of = {"path"})
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class CustomConstraintViolation implements ConstraintViolation<Void> {
    private final String message;
    private final String path;

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public Path getPropertyPath() {
        return new Path() {
            @Override
            public Iterator<Node> iterator() {
                throw new UnsupportedOperationException("not implemented");
            }

            @Override
            public String toString() {
                return CustomConstraintViolation.this.path;
            }
        };
    }

    @Override
    public String getMessageTemplate() {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Void getRootBean() {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Class<Void> getRootBeanClass() {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Object getLeafBean() {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Object[] getExecutableParameters() {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Object getExecutableReturnValue() {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Object getInvalidValue() {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public ConstraintDescriptor<?> getConstraintDescriptor() {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public <T> T unwrap(Class<T> aClass) {
        throw new UnsupportedOperationException("not implemented");
    }
}
