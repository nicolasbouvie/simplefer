package dev.nicolasbouvie.simplefer.data.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(of = {"accountNumber", "currency", "date"})
public class BalanceDto implements Dto {
    private Long id;
    private String accountNumber;
    private double amount;
    private String currency;
    private LocalDate date;
    private String type;
}
