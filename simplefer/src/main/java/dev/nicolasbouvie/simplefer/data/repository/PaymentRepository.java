package dev.nicolasbouvie.simplefer.data.repository;

import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface PaymentRepository extends PagingAndSortingRepository<Payment, Long> {
    @Query("SELECT COUNT(p) FROM Payment p WHERE p.status='FRAUD' AND p.giverAccount=?1")
    long countSuspiciousPayment(BankAccount giverAccount);

    @Query("SELECT p FROM Payment p JOIN p.giverAccount.users u" +
            " WHERE u.username=:username" +
            "   AND p.creationDate between :fromDate and :toDate")
    Page<Payment> findAllByUsernameAndCreationDateBetween(String username,
                                                          LocalDateTime fromDate, LocalDateTime toDate, Pageable pageable);

    @Query("SELECT p FROM Payment p JOIN p.giverAccount.users u" +
            " WHERE u.username=:username AND p.beneficiaryAccountNumber=:toAccountNumber" +
            "   AND p.creationDate between :fromDate and :toDate")
    Page<Payment> findAllToAccount(String username, String toAccountNumber,
                                   LocalDateTime fromDate, LocalDateTime toDate, Pageable pageable);

    @Query("SELECT p FROM Payment p, BankAccount a JOIN a.users u" +
            " WHERE p.id=:id AND u.username=:username AND a.accountNumber = p.beneficiaryAccountNumber" +
            "   AND p.status='EXECUTED' AND p.type='PAYMENT'")
    Optional<Payment> findReceivedByIdAndUsername(Long id, String username);

    @Query("SELECT p FROM Payment p JOIN p.giverAccount a JOIN a.users u" +
           " WHERE p.id=:id AND u.username=:username")
    Optional<Payment> findByIdAndUsername(Long id, String username);

    Page<Payment> findAllByStatusAndExecutionDate(
            Payment.Status status, LocalDate date, Pageable page);
}
