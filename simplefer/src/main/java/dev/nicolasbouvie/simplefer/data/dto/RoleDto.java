package dev.nicolasbouvie.simplefer.data.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Pattern;

@Data
@EqualsAndHashCode(of = {"name"})
public class RoleDto implements Dto {
    private Long id;

    @Pattern(regexp = "\\w+", message = "{simplefer.validation.role.pattern}")
    private String name;
}
