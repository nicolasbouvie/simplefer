package dev.nicolasbouvie.simplefer.data.dto;

import dev.nicolasbouvie.simplefer.data.validation.Iban;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(of = {"currency", "giverAccountNumber", "beneficiaryAccountNumber", "creationDate"})
public class PaymentDto implements Dto {

    private Long id;

    @DecimalMin(value = "0.01", message = "{simplefer.validation.amount.min}")
    @DecimalMax(value = "999999999999999.99", message = "{simplefer.validation.amount.max}")
    private double amount;

    @NotBlank(message = "{simplefer.validation.currency.required}")
    private String currency;

    @Iban
    @NotBlank(message = "{simplefer.validation.giverAccount.required}")
    private String giverAccountNumber;

    @Iban
    @NotBlank(message = "{simplefer.validation.beneficiaryAccount.required}")
    private String beneficiaryAccountNumber;

    @NotBlank(message = "{simplefer.validation.beneficiaryName.required}")
    private String beneficiaryName;

    private String communication;

    @NotNull(message = "{simplefer.validation.creationDate.required}")
    private LocalDateTime creationDate = LocalDateTime.now();

    @FutureOrPresent(message = "{simplefer.validation.creationDate.futureOrPresent}")
    private LocalDate executionDate = LocalDate.now();

    private String status;
    private String type;
}
