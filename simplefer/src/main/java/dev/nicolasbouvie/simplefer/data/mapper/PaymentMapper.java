package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.PaymentDto;
import dev.nicolasbouvie.simplefer.data.model.Payment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PaymentMapper extends ModelDtoMapper<Payment, PaymentDto> {

    @Override
    @Mapping(target = "giverAccount.accountNumber", source = "giverAccountNumber")
    @Mapping(target = "currency.code", source = "currency")
    Payment toModel(PaymentDto dto);

    @Override
    @Mapping(source = "giverAccount.accountNumber", target = "giverAccountNumber")
    @Mapping(source = "currency.code", target = "currency")
    PaymentDto toDto(Payment dto);
}
