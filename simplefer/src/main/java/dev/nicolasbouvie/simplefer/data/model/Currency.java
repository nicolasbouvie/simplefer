package dev.nicolasbouvie.simplefer.data.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;

@Entity
@Table(
    name = "CURRENCY",
    uniqueConstraints = {
        @UniqueConstraint(
            name = "CURRENCY_CODE_UK",
            columnNames = {"CODE"}
        )
    }
)
@Data
@ToString(of = {"code"})
public class Currency implements Model {

    @Id
    @SequenceGenerator(name = "CURRENCY_ID_SEQ", sequenceName = "CURRENCY_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CURRENCY_ID_SEQ")
    @Column(name = "ID", unique = true)
    private Long id;

    @Column(name = "CODE", nullable = false)
    @NotBlank(message = "Code is mandatory")
    private String code;
}
