package dev.nicolasbouvie.simplefer.data.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "PAYMENT")
@Data
@ToString(of = {"currency", "amount", "giverAccount", "beneficiaryAccountNumber"})
public class Payment implements Model {

    @Id
    @SequenceGenerator(name = "PAYMENT_ID_SEQ", sequenceName = "PAYMENT_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PAYMENT_ID_SEQ")
    @Column(name = "ID", unique = true)
    private Long id;

    @Column(name = "AMOUNT", nullable = false)
    @NotNull(message = "Amount is mandatory")
    private BigDecimal amount;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "CURRENCY_ID", nullable = false)
    @NotNull(message = "Currency is mandatory")
    private Currency currency;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "GIVER_ACCOUNT_ID", nullable = false)
    @NotNull(message = "Giver account is mandatory")
    private BankAccount giverAccount;

    @Column(name = "BENEFICIARY_ACCOUNT_NUMBER", nullable = false)
    @NotBlank(message = "Beneficiary account number is mandatory")
    private String beneficiaryAccountNumber;

    @Column(name = "BENEFICIARY_NAME", nullable = false)
    @NotBlank(message = "Beneficiary name is mandatory")
    private String beneficiaryName;

    @Column(name = "COMMUNICATION")
    private String communication;

    @Column(name = "CREATION_DATE", nullable = false, columnDefinition = "TIMESTAMP")
    @NotNull(message = "Creation date is mandatory")
    private LocalDateTime creationDate = LocalDateTime.now();

    @Column(name = "EXECUTION_DATE", nullable = false, columnDefinition = "DATE")
    @NotNull(message = "Execution date is mandatory")
    private LocalDate executionDate = LocalDate.now();

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull(message = "Status is mandatory")
    private Status status = Status.EXECUTED;

    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull(message = "Type is mandatory")
    private Type type = Type.PAYMENT;

    public enum Status {
        EXECUTED, CANCELLED, FRAUD, REJECTED, SCHEDULED;
    }

    public enum Type {
        PAYMENT, RETURN
    }
}
