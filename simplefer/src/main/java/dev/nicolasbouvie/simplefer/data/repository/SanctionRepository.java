package dev.nicolasbouvie.simplefer.data.repository;

import dev.nicolasbouvie.simplefer.data.model.Sanction;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SanctionRepository extends PagingAndSortingRepository<Sanction, Long> {
    Optional<Sanction> findByAccountNumber(String accountNumber);
}
