package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.Dto;
import dev.nicolasbouvie.simplefer.data.dto.PageDto;
import dev.nicolasbouvie.simplefer.data.model.Model;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

@Mapper(componentModel = "spring")
public interface PageMapper {

    /**
     * Map Spring Page&lt;Model&gt; to PageDto&lt;Dto&gt;.
     *
     * @param model   Object to convert
     * @param mapper  Mapper to convert typed elements from Page model
     * @param <DTO>   Target type of PageDto
     * @param <MODEL> Source type of Page model
     * @return PageDto
     */
    default <DTO extends Dto, MODEL extends Model> PageDto<DTO> toDto(Page<MODEL> model, ModelDtoMapper<MODEL, DTO> mapper) {
        Page<DTO> map = model.map(mapper::toDto);
        PageDto<DTO> dto = new PageDto<>();
        dto.setTotalPages(map.getTotalPages());
        dto.setPage(map.getNumber());
        dto.setPageSize(map.getSize());
        dto.setTotalElements(map.getTotalElements());
        dto.setNumberOfElements(map.getNumberOfElements());
        dto.setContent(map.getContent());
        return dto;
    }
}
