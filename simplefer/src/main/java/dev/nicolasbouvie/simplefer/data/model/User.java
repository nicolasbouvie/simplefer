package dev.nicolasbouvie.simplefer.data.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import java.util.Collection;

@Entity
@Table(
    name = "APP_USER",
    uniqueConstraints = {
        @UniqueConstraint(
            name = "APP_USER_USERNAME_UK",
            columnNames = {"USERNAME"}
        )
    }
)
@Data
@ToString(of = {"username", "roles"})
public class User implements Model {

    @Id
    @SequenceGenerator(name = "APP_USER_ID_SEQ", sequenceName = "APP_USER_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_USER_ID_SEQ")
    @Column(name = "ID", unique = true)
    private Long id;

    @Column(name = "USERNAME", nullable = false, unique = true)
    @NotBlank(message = "Username is mandatory")
    private String username;

    @Column(name = "PASSWORD", nullable = false)
    @NotBlank(message = "Password is mandatory")
    private String password;

    @Column(name = "ADDRESS", nullable = false)
    @NotBlank(message = "Address is mandatory")
    private String address;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "APP_USER_ROLE",
            joinColumns = {@JoinColumn(name = "APP_USER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "ROLE_ID")}
    )
    private Collection<Role> roles;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "users")
    private Collection<BankAccount> bankAccounts;
}
