package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.RoleDto;
import dev.nicolasbouvie.simplefer.data.model.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper extends ModelDtoMapper<Role, RoleDto> {
}
