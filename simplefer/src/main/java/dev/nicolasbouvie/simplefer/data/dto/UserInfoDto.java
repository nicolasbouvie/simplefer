package dev.nicolasbouvie.simplefer.data.dto;

import lombok.Data;

@Data
public class UserInfoDto implements Dto {
    private String address;
    private String oldPassword;
    private String newPassword;
}
