package dev.nicolasbouvie.simplefer.data.dto;

import dev.nicolasbouvie.simplefer.data.validation.Iban;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(of = {"accountNumber"})
public class SanctionDto implements Dto {

    private Long id;

    @Iban
    @NotBlank(message = "{simplefer.validation.sanctionAccount.required}")
    private String accountNumber;

    @NotBlank(message = "{simplefer.validation.sanctionReason.required}")
    private String reason;
}
