package dev.nicolasbouvie.simplefer.data.repository;

import dev.nicolasbouvie.simplefer.data.model.Balance;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Currency;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BalanceRepository extends PagingAndSortingRepository<Balance, Long> {
    Optional<Balance> findBalanceByCurrencyAndBankAccountAndType(Currency currency, BankAccount bankAccount, Balance.Type type);
}
