package dev.nicolasbouvie.simplefer.data.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Entity
@Table(
    name = "BANK_ACCOUNT",
    uniqueConstraints = {
        @UniqueConstraint(
            name = "BANK_ACCOUNT_ACCOUNT_NUMBER_UK",
            columnNames = {"ACCOUNT_NUMBER"}
        )
    }
)
@Data
@ToString(of = {"accountNumber", "status"})
public class BankAccount implements Model {

    @Id
    @SequenceGenerator(name = "BANK_ACCOUNT_ID_SEQ", sequenceName = "BANK_ACCOUNT_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BANK_ACCOUNT_ID_SEQ")
    @Column(name = "ID", unique = true)
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "BANK_ACCOUNT_APP_USER",
            joinColumns = {@JoinColumn(name = "BANK_ACCOUNT_ID")},
            inverseJoinColumns = {@JoinColumn(name = "APP_USER_ID")}
    )
    private Collection<User> users;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "bankAccount")
    private Collection<Balance> balances;

    @Column(name = "ACCOUNT_NUMBER", nullable = false)
    @NotBlank(message = "Account number is mandatory")
    private String accountNumber;

    @Column(name = "ACCOUNT_NAME", nullable = false)
    @NotBlank(message = "Account name is mandatory")
    private String accountName;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull(message = "Status is mandatory")
    private Status status = Status.ENABLE;

    public enum Status {
        ENABLE, BLOCKED
    }
}
