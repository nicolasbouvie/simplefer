package dev.nicolasbouvie.simplefer.data.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PaymentsFilter extends BasePageFilter {
    private LocalDate startDate;
    private LocalDate endDate;

    /**
     * Return {@link PaymentsFilter#startDate}. In case it is not set returns 1900-01-01.
     *
     * @return LocalDate
     */
    public LocalDate getStartDate() {
        return startDate == null ? LocalDate.of(1900, 1, 1) : startDate;
    }

    /**
     * Return {@link PaymentsFilter#endDate}. In case it is not set returns current date.
     *
     * @return LocalDate
     */
    public LocalDate getEndDate() {
        return endDate == null ? LocalDate.now() : endDate;
    }
}
