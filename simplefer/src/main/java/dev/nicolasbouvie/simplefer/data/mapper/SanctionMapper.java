package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.SanctionDto;
import dev.nicolasbouvie.simplefer.data.model.Sanction;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SanctionMapper extends ModelDtoMapper<Sanction, SanctionDto> {
}
