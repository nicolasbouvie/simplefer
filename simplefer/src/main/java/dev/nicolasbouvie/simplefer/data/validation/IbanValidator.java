package dev.nicolasbouvie.simplefer.data.validation;

import dev.nicolasbouvie.simplefer.service.IBANService;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * IBAN validator
 */
@RequiredArgsConstructor
public class IbanValidator implements ConstraintValidator<Iban, String> {
    private final IBANService ibanService;

    @Override
    public boolean isValid(String iban, ConstraintValidatorContext constraintValidatorContext) {
        return ibanService.isValid(iban);
    }
}
