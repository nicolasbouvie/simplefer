package dev.nicolasbouvie.simplefer.data.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;

@Entity
@Table(
    name = "SANCTION",
    uniqueConstraints = {
        @UniqueConstraint(
            name = "SANCTION_ACCOUNT_NUMBER_UK",
            columnNames = {"ACCOUNT_NUMBER"}
        )
    }
)
@Data
@ToString(of = {"accountNumber"})
public class Sanction implements Model {

    @Id
    @SequenceGenerator(name = "SANCTION_ID_SEQ", sequenceName = "SANCTION_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SANCTION_ID_SEQ")
    @Column(name = "ID", unique = true)
    private Long id;

    @Column(name = "ACCOUNT_NUMBER", nullable = false)
    @NotBlank(message = "Account number is mandatory")
    private String accountNumber;

    @Column(name = "REASON", nullable = false)
    @NotBlank(message = "Reason is mandatory")
    private String reason;
}
