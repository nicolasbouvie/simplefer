package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.BankAccountDto;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.User;
import org.mapstruct.Mapper;

import java.util.Collection;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface BankAccountMapper extends ModelDtoMapper<BankAccount, BankAccountDto> {

    default String userToString(User user) {
        return user != null ? user.getUsername() : null;
    }

    default User stringToUser(String username) {
        var user = new User();
        user.setUsername(username);
        return user;
    }
}
