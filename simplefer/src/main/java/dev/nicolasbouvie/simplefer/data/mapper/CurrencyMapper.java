package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.CurrencyDto;
import dev.nicolasbouvie.simplefer.data.model.Currency;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CurrencyMapper extends ModelDtoMapper<Currency, CurrencyDto> {
}
