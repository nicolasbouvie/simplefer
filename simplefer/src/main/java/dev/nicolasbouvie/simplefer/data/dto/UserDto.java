package dev.nicolasbouvie.simplefer.data.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Set;

@Data
@EqualsAndHashCode(of = {"username"})
public class UserDto implements Dto {

    private Long id;

    @NotBlank(message = "{simplefer.validation.username.required}")
    @Pattern(regexp = "\\w+", message = "{simplefer.validation.username.pattern}")
    private String username;

    @NotBlank(message = "{simplefer.validation.password.required}")
    private String password;

    @NotBlank(message = "{simplefer.validation.address.required}")
    private String address;

    private Set<String> roles;
    private Set<String> bankAccounts;
}
