package dev.nicolasbouvie.simplefer.data.mapper;

import dev.nicolasbouvie.simplefer.data.dto.BalanceDto;
import dev.nicolasbouvie.simplefer.data.model.Balance;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BalanceMapper extends ModelDtoMapper<Balance, BalanceDto> {

    @Override
    @Mapping(source = "accountNumber", target = "bankAccount.accountNumber")
    @Mapping(source = "currency", target = "currency.code")
    Balance toModel(BalanceDto dto);

    @Override
    @Mapping(source = "bankAccount.accountNumber", target = "accountNumber")
    @Mapping(source = "currency.code", target = "currency")
    BalanceDto toDto(Balance model);
}
