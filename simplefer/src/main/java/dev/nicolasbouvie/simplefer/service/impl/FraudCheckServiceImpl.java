package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Payment;
import dev.nicolasbouvie.simplefer.data.repository.PaymentRepository;
import dev.nicolasbouvie.simplefer.data.repository.SanctionRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import dev.nicolasbouvie.simplefer.service.FraudCheckService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class FraudCheckServiceImpl implements FraudCheckService {
    private final SanctionRepository sanctionRepository;
    private final PaymentRepository paymentRepository;
    private final ConstraintExceptionFactory constraintExceptionFactory;

    /**
     * Max number of suspicious payment before blocking account
     */
    @Value("${fraud.maxSuspiciousPayments:5}")
    private long maxSuspiciousPayments;

    @Override
    public void checkPayment(Payment payment) {
        var sanction = sanctionRepository.findByAccountNumber(payment.getBeneficiaryAccountNumber());
        if (sanction.isPresent()) {
            log.info("Payment to sanctioned account detected {}", payment);
            payment.setStatus(Payment.Status.FRAUD);
            var suspiciousPayments = paymentRepository.countSuspiciousPayment(payment.getGiverAccount());
            if (suspiciousPayments + 1 >= maxSuspiciousPayments) {
                log.warn("Account {} blocked after {} fraud tentatives", payment.getGiverAccount(), maxSuspiciousPayments);
                payment.getGiverAccount().setStatus(BankAccount.Status.BLOCKED);
            }
            paymentRepository.save(payment);
            throw constraintExceptionFactory.newConstraintValidationException("fraudCheck", sanction.get().getReason());
        }
    }
}
