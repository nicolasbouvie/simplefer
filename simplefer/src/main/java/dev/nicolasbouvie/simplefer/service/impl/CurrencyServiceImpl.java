package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.CurrencyDto;
import dev.nicolasbouvie.simplefer.data.mapper.CurrencyMapper;
import dev.nicolasbouvie.simplefer.data.model.Currency;
import dev.nicolasbouvie.simplefer.data.repository.CurrencyRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import dev.nicolasbouvie.simplefer.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {
    private final CurrencyRepository repository;
    private final CurrencyMapper mapper;
    private final ConstraintExceptionFactory constraintExceptionFactory;

    @Override
    public Page<Currency> list(BasePageFilter filter) {
        return repository.findAll(filter.getPageable("code"));
    }

    @Override
    public Currency get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> constraintExceptionFactory.newEntityNotFoundException("Currency", id));
    }

    @Override
    public Currency create(CurrencyDto dto) {
        var currency = mapper.toModel(dto);
        return repository.save(currency);
    }

    @Override
    public Currency update(CurrencyDto dto) {
        var persisted = get(dto.getId());
        var currency = mapper.toModel(dto);
        BeanUtils.copyProperties(currency, persisted);
        return repository.save(persisted);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
