package dev.nicolasbouvie.simplefer.service;

/**
 * IBAN service
 */
public interface IBANService {

    /**
     * Validate if provided IBAN is valid.
     *
     * @param iban IBAN to verify
     * @return true if valid, false otherwise
     */
    boolean isValid(String iban);
}
