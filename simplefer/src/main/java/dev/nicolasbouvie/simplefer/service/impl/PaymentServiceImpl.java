package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.dto.PaymentDto;
import dev.nicolasbouvie.simplefer.data.dto.PaymentsFilter;
import dev.nicolasbouvie.simplefer.data.mapper.PaymentMapper;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Payment;
import dev.nicolasbouvie.simplefer.data.repository.BankAccountRepository;
import dev.nicolasbouvie.simplefer.data.repository.CurrencyRepository;
import dev.nicolasbouvie.simplefer.data.repository.PaymentRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import dev.nicolasbouvie.simplefer.service.BalanceService;
import dev.nicolasbouvie.simplefer.service.FraudCheckService;
import dev.nicolasbouvie.simplefer.service.IBANService;
import dev.nicolasbouvie.simplefer.service.PaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.function.Supplier;

@Service
@RequiredArgsConstructor
@Slf4j
public class PaymentServiceImpl implements PaymentService {
    private final PaymentMapper mapper;
    private final PaymentRepository repository;
    private final BalanceService balanceService;
    private final BankAccountRepository bankAccountRepository;
    private final FraudCheckService fraudCheckService;
    private final IBANService ibanService;
    private final UserServiceImpl userService;
    private final CurrencyRepository currencyRepository;
    private final ConstraintExceptionFactory constraintExceptionFactory;

    @Value("${payment.scheduler.pageSize:100}")
    private int schedulerPageSize;

    @Override
    public Page<Payment> list(PaymentsFilter filter) {
        return repository.findAllByUsernameAndCreationDateBetween(userService.getCurrentUsername(),
                filter.getStartDate().atStartOfDay(), filter.getEndDate().plusDays(1).atStartOfDay(),
                filter.getPageable("creationDate"));
    }

    @Override
    public Page<Payment> listByBeneficiary(String accountNumber, PaymentsFilter filter) {
        if (!ibanService.isValid(accountNumber)) {
            throw constraintExceptionFactory.newConstraintValidationException(
                    "accountNumber", "simplefer.validation.iban");
        }

        return repository.findAllToAccount(userService.getCurrentUsername(), accountNumber,
                filter.getStartDate().atStartOfDay(), filter.getEndDate().plusDays(1).atStartOfDay(),
                filter.getPageable("creationDate"));
    }

    @Override
    @Transactional(noRollbackFor = {ConstraintViolationException.class}, isolation = Isolation.REPEATABLE_READ)
    public Payment create(PaymentDto dto) {
        var model = mapper.toModel(dto);
        model.setCreationDate(LocalDateTime.now());

        model.setGiverAccount(bankAccountRepository
                .findByAccountNumberAndUsers_Username(dto.getGiverAccountNumber(), userService.getCurrentUsername())
                .orElseThrow(() ->
                        constraintExceptionFactory.newConstraintValidationException(
                                //For security reason we do not inform that account does not exist in the bank
                                "giverAccountNumber",
                                "simplefer.validation.giverAccount.notFound")));
        model.setCurrency(currencyRepository.findByCode(dto.getCurrency())
                .orElseThrow(() -> constraintExceptionFactory.newConstraintValidationException(
                        "currency",
                        "simplefer.validation.currency.notFound")));

        if (Objects.equals(model.getGiverAccount().getAccountNumber(), model.getBeneficiaryAccountNumber())) {
            throw constraintExceptionFactory.newConstraintValidationException(
                    "beneficiaryAccountNumber",
                    "simplefer.validation.giverAccount.equalsBeneficiary");
        }
        if (model.getGiverAccount().getStatus() == BankAccount.Status.BLOCKED) {
            model.setStatus(Payment.Status.REJECTED);
            repository.save(model);
            throw constraintExceptionFactory.newConstraintValidationException(
                    "giverAccountNumber",
                    "simplefer.validation.giverAccount.blocked");
        }

        fraudCheckService.checkPayment(model);
        if (model.getExecutionDate().isAfter(LocalDate.now())) {
            model.setStatus(Payment.Status.SCHEDULED);
            return repository.save(model);
        }
        return executePayment(model);
    }

    @Override
    @Transactional(noRollbackFor = {ConstraintViolationException.class}, isolation = Isolation.REPEATABLE_READ)
    public Payment returnPayment(Long id) {
        var receivedPayment = repository.findReceivedByIdAndUsername(id, userService.getCurrentUsername())
                .orElseThrow(() -> constraintExceptionFactory.newEntityNotFoundException("Payment", id));

        receivedPayment.setStatus(Payment.Status.CANCELLED);
        repository.save(receivedPayment);

        var returnPayment = mapper.toDto(receivedPayment);
        returnPayment.setBeneficiaryAccountNumber(receivedPayment.getGiverAccount().getAccountNumber());
        returnPayment.setGiverAccountNumber(receivedPayment.getBeneficiaryAccountNumber());
        returnPayment.setType(Payment.Type.RETURN.name());

        return create(returnPayment);
    }

    /**
     * Execute the payment, deducting funds from giver and adding to beneficiary account (in case it is also on Simplefer)
     *
     * @param payment Payment to be executed
     * @return Executed payment
     */
    protected Payment executePayment(Payment payment) {
        var fromBalance = balanceService.getAvailableBalance(payment.getCurrency(), payment.getGiverAccount());
        if (fromBalance.getAmount().compareTo(payment.getAmount()) < 0) {
            payment.setStatus(Payment.Status.REJECTED);
            repository.save(payment);
            throw constraintExceptionFactory.newConstraintValidationException(
                    "amount",
                    "simplefer.validation.giverAccount.insufficientFunds");
        }

        fromBalance.setAmount(fromBalance.getAmount().subtract(payment.getAmount()));
        balanceService.save(fromBalance);

        var beneficiaryAccount = bankAccountRepository.findByAccountNumber(payment.getBeneficiaryAccountNumber());
        if (beneficiaryAccount.isPresent()) {
            var toBalance = balanceService.getAvailableBalance(payment.getCurrency(), beneficiaryAccount.get());
            toBalance.setAmount(toBalance.getAmount().add(payment.getAmount()));
            balanceService.save(toBalance);
        }
        payment.setStatus(Payment.Status.EXECUTED);
        return repository.save(payment);
    }

    @Override
    public void deleteScheduled(Long id) {
        Payment payment = repository.findByIdAndUsername(id, userService.getCurrentUsername())
                .orElseThrow(() -> constraintExceptionFactory.newEntityNotFoundException("Payment", id));
        if (payment.getStatus() != Payment.Status.SCHEDULED) {
            throw constraintExceptionFactory.newConstraintValidationException(
                    "status", "simplefer.validation.payment.statusNotScheduled", payment.getStatus());
        }
        repository.delete(payment);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW,
                   noRollbackFor = {ConstraintViolationException.class},
            isolation = Isolation.REPEATABLE_READ)
    public void executeScheduledPayment(Payment model) {
        try {
            if (model.getGiverAccount().getStatus() == BankAccount.Status.BLOCKED) {
                log.info("Scheduled payment {} rejected as giver account is blocked", model);
                model.setStatus(Payment.Status.REJECTED);
                repository.save(model);
                return;
            }

            fraudCheckService.checkPayment(model);
            executePayment(model);
        } catch (ConstraintViolationException e) {
            log.info("Scheduled payment not executed due to constraint violation", e);
        }
    }

    /**
     * This method executes every 30 min after latest execution and queries for all payments with status SCHEDULED on current day.
     * Each query execution is limited to {@link PaymentServiceImpl#schedulerPageSize} records ordered by creation date.
     * Each payment is processed in a new transaction, therefore if this method is interrupted for any reason it will have
     * another change to process the pending scheduled payments in next execution.
     */
    @Scheduled(fixedDelay = 1800000)
    public void executeScheduledPayments() {
        log.info("Start executeScheduledPayments");
        Supplier<Page<Payment>> scheduledPaymentsQuery = () -> {
            var page = repository.findAllByStatusAndExecutionDate(
                        Payment.Status.SCHEDULED,
                        LocalDate.now(),
                        PageRequest.of(0, schedulerPageSize, Sort.by("creationDate")));
            log.info("Starting execution of {} from {} scheduled payments", page.getNumberOfElements(), page.getTotalElements());
            return page;
        };

        Page<Payment> page;
        while ((page = scheduledPaymentsQuery.get()).hasContent()) {
            page.getContent().forEach(this::executeScheduledPayment);
        }
        log.info("End executeScheduledPayments");
    }
}
