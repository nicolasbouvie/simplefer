package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.UserDto;
import dev.nicolasbouvie.simplefer.data.dto.UserInfoDto;
import dev.nicolasbouvie.simplefer.data.mapper.UserMapper;
import dev.nicolasbouvie.simplefer.data.model.User;
import dev.nicolasbouvie.simplefer.data.repository.RoleRepository;
import dev.nicolasbouvie.simplefer.data.repository.UserRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import dev.nicolasbouvie.simplefer.service.SecurityService;
import dev.nicolasbouvie.simplefer.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserMapper mapper;
    private final UserRepository repository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenStore tokenStore;
    private final ConsumerTokenServices tokenServices;
    private final ConstraintExceptionFactory constraintExceptionFactory;
    private final SecurityService securityService;

    @Override
    public Page<User> list(BasePageFilter filter) {
        return repository.findAll(filter.getPageable("id"));
    }

    @Override
    public User get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> constraintExceptionFactory.newEntityNotFoundException("User", id));
    }

    @Override
    public String getCurrentUsername() {
        return securityService.getAuthentication().getName();
    }

    @Override
    public User create(UserDto dto) {
        var user = mapper.toModel(dto);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        if (dto.getRoles() == null || dto.getRoles().isEmpty()) {
            dto.setRoles(Set.of("USER"));
        }
        user.setRoles(dto.getRoles().stream().map(r->roleRepository.findByName(r).orElseThrow(
            () -> constraintExceptionFactory.newEntityNotFoundException("Role", r)
        )).collect(Collectors.toList()));

        return repository.save(user);
    }

    @Override
    public User updateInfo(UserInfoDto dto) {
        var username = getCurrentUsername();
        var persisted = repository.findByUsername(username)
                .orElseThrow(() -> constraintExceptionFactory.newEntityNotFoundException("User", username));
        if (dto.getAddress() != null) {
            persisted.setAddress(dto.getAddress());
        }
        if (dto.getNewPassword() != null) {
            if (!passwordEncoder.matches(dto.getOldPassword(), persisted.getPassword())) {
                throw constraintExceptionFactory.newConstraintValidationException(
                        "oldPassword", "simplefer.validation.password.doesntMatch");
            }
            persisted.setPassword(passwordEncoder.encode(dto.getNewPassword()));
        }

        return repository.save(persisted);
    }

    @Override
    public void logout() {
        var token = tokenStore.getAccessToken(securityService.getAuthentication()).getValue();
        tokenServices.revokeToken(token);
    }
}
