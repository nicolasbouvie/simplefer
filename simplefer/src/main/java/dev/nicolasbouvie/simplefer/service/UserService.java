package dev.nicolasbouvie.simplefer.service;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.UserDto;
import dev.nicolasbouvie.simplefer.data.dto.UserInfoDto;
import dev.nicolasbouvie.simplefer.data.model.User;
import org.springframework.data.domain.Page;

/**
 * User service
 */
public interface UserService {

    /**
     * List available Users.
     *
     * @param filter Pagination filter
     * @return Paged result of User
     */
    Page<User> list(BasePageFilter filter);

    /**
     * Retrieve one User by ID.
     *
     * @param id ID to retrieve
     * @return User
     */
    User get(Long id);

    /**
     * Retrieve the username of authenticated user.
     *
     * @return username
     */
    String getCurrentUsername();

    /**
     * Create a single User.
     *
     * @param dto User to create
     * @return Persisted User
     */
    User create(UserDto dto);

    /**
     * Update the authenticate user basic information.
     *
     * @param dto User information to update
     * @return Persisted User
     */
    User updateInfo(UserInfoDto dto);

    /**
     * Logout authenticate user, invalidate access and refresh tokens.
     */
    void logout();
}
