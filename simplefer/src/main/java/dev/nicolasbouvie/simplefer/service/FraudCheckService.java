package dev.nicolasbouvie.simplefer.service;

import dev.nicolasbouvie.simplefer.data.model.Payment;

import javax.validation.ConstraintViolationException;

/**
 * Fraud check service
 */
public interface FraudCheckService {

    /**
     * Verify if a payment may be fraudulent.
     * In case a fraud is detected {@link ConstraintViolationException} is thrown
     * and payment status is set to {@link Payment.Status#FRAUD}.
     *
     * @param payment Payment to verify
     * @throws ConstraintViolationException In case a fraud is detected
     */
    void checkPayment(Payment payment) throws ConstraintViolationException;
}
