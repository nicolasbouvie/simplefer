package dev.nicolasbouvie.simplefer.service;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.CurrencyDto;
import dev.nicolasbouvie.simplefer.data.model.Currency;
import org.springframework.data.domain.Page;

/**
 * Currency service
 */
public interface CurrencyService {

    /**
     * List available currencies.
     *
     * @param filter Pagination filter
     * @return Paged result of Currency
     */
    Page<Currency> list(BasePageFilter filter);

    /**
     * Retrieve one currency by ID.
     *
     * @param id ID to retrieve
     * @return Currency
     */
    Currency get(Long id);

    /**
     * Create a single currency.
     *
     * @param dto Currency to create
     * @return Persisted Currency
     */
    Currency create(CurrencyDto dto);

    /**
     * Updated a single currency.
     *
     * @param dto Currency to updated
     * @return Persisted Currency
     */
    Currency update(CurrencyDto dto);

    /**
     * Delete a currency by ID.
     *
     * @param id Currency ID
     */
    void delete(Long id);
}
