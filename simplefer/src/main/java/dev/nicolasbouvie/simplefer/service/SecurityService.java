package dev.nicolasbouvie.simplefer.service;

import org.springframework.security.oauth2.provider.OAuth2Authentication;

/**
 * Security service
 */
public interface SecurityService {

    /**
     * Return the current authentication context
     * @return OAuth2Authentication context
     */
    OAuth2Authentication getAuthentication();
}
