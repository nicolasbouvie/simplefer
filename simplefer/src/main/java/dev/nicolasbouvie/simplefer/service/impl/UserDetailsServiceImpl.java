package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.repository.UserRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final Pattern usernamePattern = Pattern.compile("\\w+");
    private final UserRepository repository;
    private final ConstraintExceptionFactory constraintExceptionFactory;

    @Override
    public UserDetails loadUserByUsername(String username) {
        if (!usernamePattern.matcher(username).matches()) {
            throw constraintExceptionFactory.newConstraintValidationException("username", "simplefer.validation.username.pattern");
        }
        var user = repository.findByUsername(username)
                .orElseThrow(() -> constraintExceptionFactory.newEntityNotFoundException("User", username));
        var authorities = user.getRoles().stream()
                .map(r -> new SimpleGrantedAuthority(r.getName()))
                .collect(Collectors.toList());
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                authorities);
    }
}
