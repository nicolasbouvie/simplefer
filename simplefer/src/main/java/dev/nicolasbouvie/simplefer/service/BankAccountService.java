package dev.nicolasbouvie.simplefer.service;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import org.springframework.data.domain.Page;

/**
 * Bank account service
 */
public interface BankAccountService {

    /**
     * List all bank accounts of a specific user.
     *
     * @param username User to list accounts
     * @param filter   Pagination filter
     * @return Paged result of BankAccount
     */
    Page<BankAccount> listByUser(String username, BasePageFilter filter);

    /**
     * List all bank accounts of current authenticated user.
     *
     * @param filter Pagination filter
     * @return Paged result of BankAccount
     */
    Page<BankAccount> list(BasePageFilter filter);
}
