package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.repository.BankAccountRepository;
import dev.nicolasbouvie.simplefer.service.BankAccountService;
import dev.nicolasbouvie.simplefer.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BankAccountServiceImpl implements BankAccountService {
    private final BankAccountRepository repository;
    private final UserService userService;

    @Override
    public Page<BankAccount> listByUser(String username, BasePageFilter filter) {
        return repository.findAllByUsers_Username(username, filter.getPageable("accountName"));
    }

    @Override
    public Page<BankAccount> list(BasePageFilter filter) {
        return listByUser(userService.getCurrentUsername(), filter);
    }
}
