package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.model.Balance;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Currency;
import dev.nicolasbouvie.simplefer.data.repository.BalanceRepository;
import dev.nicolasbouvie.simplefer.service.BalanceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class BalanceServiceImpl implements BalanceService {
    private final BalanceRepository repository;

    @Override
    public Balance getAvailableBalance(Currency currency, BankAccount bankAccount) {
        var balance = repository.findBalanceByCurrencyAndBankAccountAndType(currency, bankAccount, Balance.Type.AVAILABLE);
        if (balance.isEmpty()) {
            var available = new Balance();
            available.setCurrency(currency);
            available.setBankAccount(bankAccount);
            return repository.save(available);
        } else if (balance.get().getDate().isBefore(LocalDate.now())) {
            Balance eodBalance = balance.get();
            eodBalance.setType(Balance.Type.END_OF_DAY);
            repository.save(eodBalance);

            var available = new Balance();
            available.setCurrency(currency);
            available.setBankAccount(bankAccount);
            available.setAmount(eodBalance.getAmount());
            return repository.save(available);
        }
        return balance.get();
    }

    @Override
    public Balance save(Balance balance) {
        return repository.save(balance);
    }
}
