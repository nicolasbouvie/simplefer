package dev.nicolasbouvie.simplefer.service;

import dev.nicolasbouvie.simplefer.data.model.Balance;
import dev.nicolasbouvie.simplefer.data.model.BankAccount;
import dev.nicolasbouvie.simplefer.data.model.Currency;

/**
 * Balance service
 */
public interface BalanceService {

    /**
     * Retrieve the current available currency balance of specific account.
     * If latest balance is in past date, update it as end of day balance,
     * and return new balance for current day.
     *
     * @param currency Currency of balance
     * @param bankAccount Source account
     * @return Available Balance
     */
    Balance getAvailableBalance(Currency currency, BankAccount bankAccount);

    /**
     * Save balance record
     * @param balance Model to persist
     * @return Persisted Balance
     */
    Balance save(Balance balance);
}
