package dev.nicolasbouvie.simplefer.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import dev.nicolasbouvie.simplefer.service.IBANService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class IBANServiceImpl implements IBANService {
    private final RestTemplate restTemplate;

    /**
     * Url of external system responsible to validate IBAN
     */
    @Value("${external.ibanValidationUrl}")
    private String ibanValidationUrl;

    @Override
    public boolean isValid(String iban) {
        var response = restTemplate.getForObject(ibanValidationUrl, JsonNode.class, Map.of("iban", iban));
        return response != null && response.has("valid")
                && response.get("valid").asBoolean(false);
    }
}
