package dev.nicolasbouvie.simplefer.service.impl;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.SanctionDto;
import dev.nicolasbouvie.simplefer.data.mapper.SanctionMapper;
import dev.nicolasbouvie.simplefer.data.model.Sanction;
import dev.nicolasbouvie.simplefer.data.repository.SanctionRepository;
import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import dev.nicolasbouvie.simplefer.service.SanctionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SanctionServiceImpl implements SanctionService {
    private final SanctionRepository repository;
    private final SanctionMapper mapper;
    private final ConstraintExceptionFactory constraintExceptionFactory;

    @Override
    public Page<Sanction> list(BasePageFilter filter) {
        return repository.findAll(filter.getPageable("id"));
    }

    @Override
    public Sanction get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> constraintExceptionFactory.newEntityNotFoundException("Sanction", id));
    }

    @Override
    public Sanction create(SanctionDto dto) {
        Sanction sanction = mapper.toModel(dto);
        return repository.save(sanction);
    }

    @Override
    public Sanction update(SanctionDto dto) {
        var persisted = get(dto.getId());
        var sanction = mapper.toModel(dto);
        BeanUtils.copyProperties(sanction, persisted);
        return repository.save(persisted);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }
}
