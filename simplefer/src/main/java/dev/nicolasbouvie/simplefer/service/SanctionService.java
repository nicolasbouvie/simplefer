package dev.nicolasbouvie.simplefer.service;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.SanctionDto;
import dev.nicolasbouvie.simplefer.data.model.Sanction;
import org.springframework.data.domain.Page;

/**
 * Sanction service
 */
public interface SanctionService {

    /**
     * List available Sanctions.
     *
     * @param filter Pagination filter
     * @return Paged result of Sanction
     */
    Page<Sanction> list(BasePageFilter filter);

    /**
     * Retrieve one Sanction by ID.
     *
     * @param id ID to retrieve
     * @return Sanction
     */
    Sanction get(Long id);

    /**
     * Create a single Sanction.
     *
     * @param dto Sanction to create
     * @return Persisted Sanction
     */
    Sanction create(SanctionDto dto);

    /**
     * Update a single Sanction.
     *
     * @param dto Sanction to update
     * @return Persisted Sanction
     */
    Sanction update(SanctionDto dto);

    /**
     * Delete a Sanction by ID.
     *
     * @param id Sanction ID
     */
    void delete(Long id);
}
