package dev.nicolasbouvie.simplefer.service;

import dev.nicolasbouvie.simplefer.data.dto.PaymentDto;
import dev.nicolasbouvie.simplefer.data.dto.PaymentsFilter;
import dev.nicolasbouvie.simplefer.data.model.Payment;
import org.springframework.data.domain.Page;

/**
 * Payment service
 */
public interface PaymentService {

    /**
     * List payments executed by authenticated user.
     *
     * @param filter Pagination filter
     * @return Paged result of Payment
     */
    Page<Payment> list(PaymentsFilter filter);

    /**
     * List payments executed by authenticated user to specific beneficiary.
     *
     * @param accountNumber Beneficiary account number
     * @param filter        Pagination filter
     * @return Paged result of Payment
     */
    Page<Payment> listByBeneficiary(String accountNumber, PaymentsFilter filter);

    /**
     * Create and execute a new payment. The giver account must belong to authenticated user.
     * If beneficiary also belongs to Simplefer, the funds are transferred between the two accounts,
     * otherwise only deduced from giver account.
     *
     * @param dto Payment to create
     * @return Persisted Payment
     */
    Payment create(PaymentDto dto);

    /**
     * Return a payment that authenticated user is beneficiary.
     * Funds are reverted to giver account and deduced from beneficiary.
     *
     * @param id ID of payment to return
     * @return Persisted r-payment
     */
    Payment returnPayment(Long id);

    /**
     * Delete a scheduled payment by ID
     * @param id Payment id to delete
     */
    void deleteScheduled(Long id);
}
