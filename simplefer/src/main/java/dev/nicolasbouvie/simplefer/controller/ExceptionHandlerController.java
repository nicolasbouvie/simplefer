package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.validation.ConstraintExceptionFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RestControllerAdvice
@RequiredArgsConstructor
public class ExceptionHandlerController {

    private final Pattern entityNotFoundPattern = Pattern.compile("No class ([^\\s]+) entity with id (\\d+) exists!");
    private final ConstraintExceptionFactory constraintExceptionFactory;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return ex.getBindingResult().getAllErrors().stream()
                .collect(Collectors.toMap(o -> ((FieldError) o).getField(), ObjectError::getDefaultMessage));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public Map<String, String> handleValidationExceptions(ConstraintViolationException ex) {
        return ex.getConstraintViolations().stream()
                .collect(Collectors.toMap(o -> o.getPropertyPath().toString(), ConstraintViolation::getMessage));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public Map<String, String> handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
        if (ex.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
            var cv = (org.hibernate.exception.ConstraintViolationException) ex.getCause();
            switch (cv.getSQLException().getSQLState()) {
                case "23505":
                    return handleValidationExceptions(
                        constraintExceptionFactory.newConstraintValidationException("duplicate", "simplefer.validation.entity.duplicate")
                    );
                case "23503":
                    return handleValidationExceptions(
                        constraintExceptionFactory.newConstraintValidationException("dataIntegrity", "simplefer.validation.entity.delete.fk")
                    );
                default:
            }

        }
        return handleValidationExceptions(
            constraintExceptionFactory.newConstraintValidationException("dataIntegrity", "simplefer.validation.entity.unexpected")
        );
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    public Map<String, String> handleEntityNotFoundException(EntityNotFoundException ex) {
        return Map.of("entity", ex.getLocalizedMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EmptyResultDataAccessException.class)
    public Map<String, String> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex) {
        Matcher matcher = entityNotFoundPattern.matcher(ex.getLocalizedMessage());
        if (matcher.matches()) {
            var modelName = matcher.group(1).substring(matcher.group(1).lastIndexOf('.')+1);
            var id = matcher.group(2);
            return handleEntityNotFoundException(constraintExceptionFactory.newEntityNotFoundException(modelName, id));
        }
        return handleValidationExceptions(
                constraintExceptionFactory.newConstraintValidationException("dataIntegrity", "simplefer.validation.entity.unexpected")
        );
    }
}
