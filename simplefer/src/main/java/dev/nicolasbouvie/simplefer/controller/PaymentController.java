package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.dto.PageDto;
import dev.nicolasbouvie.simplefer.data.dto.PaymentDto;
import dev.nicolasbouvie.simplefer.data.dto.PaymentsFilter;
import dev.nicolasbouvie.simplefer.data.mapper.PaymentMapper;
import dev.nicolasbouvie.simplefer.data.model.Payment;
import dev.nicolasbouvie.simplefer.service.PaymentService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class PaymentController extends AbstractController<PaymentService, PaymentMapper> {

    @GetMapping(path = "/api/payments", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "List Own Payments",
            description = "Retrieves a list payments created by current authenticated user")
    public PageDto<PaymentDto> list(PaymentsFilter filter) {
        var currenciesPage = service.list(filter);
        return pageMapper.toDto(currenciesPage, mapper);
    }

    @GetMapping(path = "/api/payments/beneficiaryAccount/{accountNumber}",
                produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "List Own Payments by Beneficiary",
            description = "Retrieves a list payments created by current authenticated user filtered by beneficiary account")
    public PageDto<PaymentDto> listByBeneficiary(@PathVariable String accountNumber, PaymentsFilter filter) {
        var currenciesPage = service.listByBeneficiary(accountNumber, filter);
        return pageMapper.toDto(currenciesPage, mapper);
    }

    @PostMapping(path = "/api/payments", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create Payment",
            description = "Create a new payment. If the payment is future schedule it, otherwise execute right away")
    public PaymentDto create(@Valid @RequestBody PaymentDto dto) {
        dto.setType(Payment.Type.PAYMENT.name());
        dto.setId(null);
        var payment = service.create(dto);
        return mapper.toDto(payment);
    }

    @PutMapping(path = "/api/payments/{id}/return", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return Payment",
            description = "Return a payment that current authenticate user is beneficiary")
    public PaymentDto returnPayment(@PathVariable Long id) {
        var payment = service.returnPayment(id);
        return mapper.toDto(payment);
    }

    @DeleteMapping(path = "/api/payments/{id}")
    @Operation(summary = "Delete Payment",
            description = "Delete a scheduled payment by ID for current authenticated user")
    public void deleteScheduled(@PathVariable Long id) {
        service.deleteScheduled(id);
    }
}
