package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.mapper.PageMapper;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractController<SERVICE, MAPPER> {
    protected SERVICE service;
    protected MAPPER mapper;
    protected PageMapper pageMapper;

    @Autowired
    public void setService(SERVICE service) {
        this.service = service;
    }

    @Autowired
    public void setMapper(MAPPER mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setPageMapper(PageMapper pageMapper) {
        this.pageMapper = pageMapper;
    }
}
