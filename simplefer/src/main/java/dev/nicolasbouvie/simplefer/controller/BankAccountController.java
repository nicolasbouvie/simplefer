package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.dto.BankAccountDto;
import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.PageDto;
import dev.nicolasbouvie.simplefer.data.mapper.BankAccountMapper;
import dev.nicolasbouvie.simplefer.service.BankAccountService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BankAccountController extends AbstractController<BankAccountService, BankAccountMapper> {

    @GetMapping(path = "/api/accounts", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "List Own Bank Accounts",
        description = "Retrieves a list of Bank accounts for current authenticated user")
    public PageDto<BankAccountDto> list(BasePageFilter filter) {
        var bankAccountsPage = service.list(filter);
        return pageMapper.toDto(bankAccountsPage, mapper);
    }

    @GetMapping(path = "/api/accounts/{username}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "List User Bank Accounts",
            description = "Retrieves a list of Bank accounts for a specific user")
    @PreAuthorize("hasAuthority('ADMIN')")
    public PageDto<BankAccountDto> listUserAccounts(BasePageFilter filter, @PathVariable String username) {
        var bankAccountsPage = service.listByUser(username, filter);
        return pageMapper.toDto(bankAccountsPage, mapper);
    }
}
