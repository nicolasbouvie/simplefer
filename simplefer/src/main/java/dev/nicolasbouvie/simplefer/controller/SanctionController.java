package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.PageDto;
import dev.nicolasbouvie.simplefer.data.dto.SanctionDto;
import dev.nicolasbouvie.simplefer.data.mapper.SanctionMapper;
import dev.nicolasbouvie.simplefer.service.SanctionService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@PreAuthorize("hasAuthority('ADMIN')")
public class SanctionController extends AbstractController<SanctionService, SanctionMapper> {

    @GetMapping(path = "/api/sanctions", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "List Sanctions",
            description = "Retrieves a list current sanctions")
    public PageDto<SanctionDto> list(BasePageFilter filter) {
        var sanctionsPage = service.list(filter);
        return pageMapper.toDto(sanctionsPage, mapper);
    }

    @GetMapping(path = "/api/sanctions/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get Sanction by ID",
            description = "Retrieves a sanction by its ID")
    public SanctionDto get(@PathVariable Long id) {
        var sanction = service.get(id);
        return mapper.toDto(sanction);
    }

    @PostMapping(path = "/api/sanctions", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create Sanction",
            description = "Create a single new sanction")
    public SanctionDto create(@Valid @RequestBody SanctionDto dto) {
        dto.setId(null);
        var sanction = service.create(dto);
        return mapper.toDto(sanction);
    }

    @PutMapping(path = "/api/sanctions/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Update Sanction",
            description = "Update a single sanction by its ID")
    public SanctionDto update(@Valid @RequestBody SanctionDto dto, @PathVariable Long id) {
        dto.setId(id);
        var sanction = service.update(dto);
        return mapper.toDto(sanction);
    }

    @DeleteMapping(path = "/api/sanctions/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Delete Sanction by ID",
            description = "Delete a single sanction by its ID")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
