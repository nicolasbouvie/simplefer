package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.PageDto;
import dev.nicolasbouvie.simplefer.data.dto.UserDto;
import dev.nicolasbouvie.simplefer.data.dto.UserInfoDto;
import dev.nicolasbouvie.simplefer.data.mapper.UserMapper;
import dev.nicolasbouvie.simplefer.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class UserController extends AbstractController<UserService, UserMapper> {

    @GetMapping(path = "/api/users", produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(summary = "List Users",
            description = "Retrieves a paged list of users")
    public PageDto<UserDto> list(BasePageFilter filter) {
        var userPage = service.list(filter);
        return pageMapper.toDto(userPage, mapper);
    }

    @GetMapping(path = "/api/users/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(summary = "Get User by ID",
            description = "Retrieves a single user by its ID")
    public UserDto get(@PathVariable Long id) {
        var user = service.get(id);
        return mapper.toDto(user);
    }

    @PostMapping(path = "/api/users", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(summary = "Create User",
            description = "Create a single new user")
    public UserDto create(@Valid @RequestBody UserDto dto) {
        dto.setId(null);
        var user = service.create(dto);
        return mapper.toDto(user);
    }

    @PutMapping(path = "/api/users", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Update User Information",
            description = "Update user information of current authenticated user")
    public UserDto updateInfo(@Valid @RequestBody UserInfoDto dto) {
        var user = service.updateInfo(dto);
        return mapper.toDto(user);
    }

    @PostMapping(path = "/api/logout")
    @Operation(summary = "Logout",
            description = "Logout from application, revoking authentication and refresh tokens of current authenticated user")
    public void logout() {
        service.logout();
    }
}
