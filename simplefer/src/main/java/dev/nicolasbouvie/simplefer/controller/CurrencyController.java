package dev.nicolasbouvie.simplefer.controller;

import dev.nicolasbouvie.simplefer.data.dto.BasePageFilter;
import dev.nicolasbouvie.simplefer.data.dto.CurrencyDto;
import dev.nicolasbouvie.simplefer.data.dto.PageDto;
import dev.nicolasbouvie.simplefer.data.mapper.CurrencyMapper;
import dev.nicolasbouvie.simplefer.service.CurrencyService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@PreAuthorize("hasAuthority('ADMIN')")
public class CurrencyController extends AbstractController<CurrencyService, CurrencyMapper> {

    @GetMapping(path = "/api/currencies", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "List Currencies",
            description = "Retrieves a list of available currencies")
    public PageDto<CurrencyDto> list(BasePageFilter filter) {
        var currenciesPage = service.list(filter);
        return pageMapper.toDto(currenciesPage, mapper);
    }

    @GetMapping(path = "/api/currencies/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get Currency by ID",
            description = "Retrieves a single currency by its ID")
    public CurrencyDto get(@PathVariable Long id) {
        var currency = service.get(id);
        return mapper.toDto(currency);
    }

    @PostMapping(path = "/api/currencies", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create Currency",
            description = "Create a single new currency")
    public CurrencyDto create(@Valid @RequestBody CurrencyDto dto) {
        dto.setId(null);
        var currency = service.create(dto);
        return mapper.toDto(currency);
    }

    @PutMapping(path = "/api/currencies/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Update Currency by ID",
            description = "Update a single currency by its ID")
    public CurrencyDto update(@Valid @RequestBody CurrencyDto dto, @PathVariable Long id) {
        dto.setId(id);
        var currency = service.update(dto);
        return mapper.toDto(currency);
    }

    @DeleteMapping(path = "/api/currencies/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Delete Currency by ID",
            description = "Delete a single currency by its ID")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
